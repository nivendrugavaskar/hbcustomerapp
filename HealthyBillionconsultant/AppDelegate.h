//
//  AppDelegate.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 05/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"
#import "SVProgressHUD.h"
#import "UIKit+AFNetworking.h"
#import "NSString+Validation.h"
#import "UIImage+Rotateimage.h"
#import "SWRevealViewController.h"
#import "UINavigationBar+customnavigation.h"
#import "SHLineGraphView.h"
#import "SHPlot.h"
#import "MarqueeLabel.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#pragma create an invisible button over front view when side table is open at that time this button will active so that we can 1st hide side bar and then can do other thing
UIButton *hiderevealButton;
CGRect globalscreensize;
BOOL isorientation;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
@public
    NSString *parentUrl,*customerusername,*customertoken,*customersessionname,*customerpwd,*customeraccesskey,*MD5string;
    BOOL userdefaultcheck,registerdfirst,noschedulefound;
    NSMutableDictionary *globalcustomerdetail;
     
}

@property (strong, nonatomic) UIWindow *window;


@end

