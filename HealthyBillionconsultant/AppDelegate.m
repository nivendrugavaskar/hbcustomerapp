//
//  AppDelegate.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 05/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "AppDelegate.h"
#import "passwordViewController.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    [NSThread sleepForTimeInterval:1.0];
   // parentUrl=@"http://dev1.businessprodemo.com/hbcrm/php/webservice.php?operation=";
   // http://test2.businessprodemo.com/HBCRM/php/
    parentUrl=@"http://healthybillions.com/crm/webservice.php?operation=";
   //  parentUrl=@"http://test2.businessprodemo.com/HBCRM/php/webservice.php?operation=";
    globalscreensize = [[UIScreen mainScreen] bounds];
#pragma set root view controller according to userdefault
    
    NSUserDefaults *default1=[NSUserDefaults standardUserDefaults];
    if([default1 valueForKey:@"customerusername"])
    {
    UIStoryboard *MainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    UINavigationController *controller = (UINavigationController*)[MainStoryboard
                                                                   instantiateViewControllerWithIdentifier: @"RootNavigationController"];
    passwordViewController *login=[MainStoryboard instantiateViewControllerWithIdentifier:@"PasswordViewController"];
    [controller setViewControllers:[NSArray arrayWithObject:login] animated:YES];
    self.window.rootViewController=controller;
    userdefaultcheck=TRUE;
    // fetch before values
         registerdfirst=[[NSUserDefaults standardUserDefaults] boolForKey:@"registered"];
        customerusername=[default1 valueForKey:@"customerusername"];
        customersessionname=[default1 valueForKey:@"customersession"];
        customerpwd=[default1 valueForKey:@"customerpassword"];
        customeraccesskey=[default1 valueForKey:@"customeraccesskey"];
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//- (NSUInteger) application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
//{
//    if(isorientation)
//    {
//        return UIInterfaceOrientationMaskLandscapeRight|UIInterfaceOrientationLandscapeLeft;
//    }
//    return UIInterfaceOrientationMaskPortrait;
//}
-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if(isorientation)
    {
        return UIInterfaceOrientationMaskLandscapeRight|UIInterfaceOrientationLandscapeLeft;
    }
    return (UIInterfaceOrientationMaskPortrait);
}
@end
