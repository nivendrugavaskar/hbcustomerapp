

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@protocol CalendarDelegate <NSObject>

-(void)tappedOnDate:(NSDate *)selectedDate;

-(void)getmonthDetailon_Swipe:(BOOL)hiddenStaus;


@end

@interface CalendarView : UIView
{
    NSInteger _selectedDate,_todaydate,_todaymonth,_todayyear;
    NSArray *_weekNames;
    @public
    NSMutableArray *scheduleddate,*datearray,*montharray,*yeararray;
    
    NSMutableArray *getsessionStatusArr;
    
    AppDelegate *app;
}

@property (nonatomic,strong) NSDate *calendarDate;
@property (nonatomic,weak) id<CalendarDelegate> delegate;

@end
