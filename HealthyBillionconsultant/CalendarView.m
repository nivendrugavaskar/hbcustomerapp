
#import "CalendarView.h"
#import "SVProgressHUD.h"

@interface CalendarView()

{
    
    NSCalendar *gregorian;
    NSInteger _selectedMonth;
    NSInteger _selectedYear;
    UIButton *next,*previous;
    UIImageView *greenview;
    
    BOOL loadermaintain;
    
   
    
}

@end
@implementation CalendarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        app=(AppDelegate *)[UIApplication sharedApplication].delegate;
        
        UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
        swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
        [self addGestureRecognizer:swipeleft];
        UISwipeGestureRecognizer * swipeRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
        swipeRight.direction=UISwipeGestureRecognizerDirectionRight;
        [self addGestureRecognizer:swipeRight];
//        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(self.bounds.origin.x, self.bounds.size.height-100, self.bounds.size.width, 44)];
//        [label setBackgroundColor:[UIColor blackColor]];
//        [label setTextColor:[UIColor whiteColor]];
//        [label setText:@"swipe to change months"];
//        label.textAlignment = NSTextAlignmentCenter;
//        [self addSubview:label];
//        [UILabel beginAnimations:NULL context:nil];
//        [UILabel setAnimationDuration:2.0];
//        [label setAlpha:0];
//        [UILabel commitAnimations];
        
        greenview=[[UIImageView alloc] init];
    }
//    UIImageView *bgimageview=[[UIImageView alloc]initWithFrame:frame];
//    bgimageview.image=[UIImage imageNamed:@"bg.png"];
//    [self addSubview:bgimageview];
    self.backgroundColor=[UIColor clearColor];
    return self;
}

-(void)hudload
{
 [SVProgressHUD showWithStatus:@"Loading...."];
}


-(void)nextmonth
{
    //[self performSelectorInBackground:@selector(hudload) withObject:nil];
   // loadermaintain=TRUE;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    components.day = 1;
    components.month += 1;
    self.calendarDate = [gregorian dateFromComponents:components];
    [UIView transitionWithView:self
                      duration:0.0f
                       options:UIViewAnimationOptionCurveEaseInOut
                    animations:^ {
                        [self setNeedsDisplay];
                        [SVProgressHUD dismiss];
                    }
                    completion:nil];

}
-(void)previousmonth
{
   // [self performSelectorInBackground:@selector(hudload) withObject:nil];
    //loadermaintain=TRUE;
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    components.day = 1;
    components.month -= 1;
    self.calendarDate = [gregorian dateFromComponents:components];
    [UIView transitionWithView:self
                      duration:0.0f
                       options:UIViewAnimationOptionCurveEaseInOut
                    animations:^
    {
        [self setNeedsDisplay];
        [SVProgressHUD dismiss];
    }
                    completion:nil];

}

- (void)drawRect:(CGRect)rect
{
    app->noschedulefound=FALSE;
   // NSLog(@"Datecalender=%@",self.calendarDate);
    NSInteger columns = 7;
    NSInteger width = (self.frame.size.width-40)/7;
    NSInteger originX = 20;
    NSInteger originY = 60;
    NSInteger height=30;
    
    next=[UIButton buttonWithType:UIButtonTypeCustom];
    next.frame=CGRectMake(self.frame.size.width-80, 25,70, 30);
   // next.backgroundColor=[UIColor redColor];
    [next addTarget:self action:@selector(nextmonth) forControlEvents:UIControlEventTouchUpInside];
    UIImageView *arrowimage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_arrow.png"]];
    //arrowimage.backgroundColor=[UIColor yellowColor];
    arrowimage.frame=CGRectMake(next.frame.size.width-30, 5, 20, 20);
    arrowimage.contentMode=UIViewContentModeScaleAspectFit;
    [next addSubview:arrowimage];
    [self addSubview:next];
    
    previous=[UIButton buttonWithType:UIButtonTypeCustom];
    previous.frame=CGRectMake(originX-10, 25,70, 30);
   // previous.backgroundColor=[UIColor redColor];
    UIImageView *arrowimage1=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"left_arrow.png"]];
    arrowimage1.frame=CGRectMake(previous.frame.origin.x, 5, 20, 20);
    arrowimage1.contentMode=UIViewContentModeScaleAspectFit;
    [previous addSubview:arrowimage1];
    [previous addTarget:self action:@selector(previousmonth) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:previous];
    
    [self setCalendarParameters];
    _weekNames = @[@"MON",@"TUE",@"WED",@"THU",@"FRI",@"SAT",@"SUN"];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
//    _selectedDate  =components.day;
    components.day = 1;
    NSDate *firstDayOfMonth = [gregorian dateFromComponents:components];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:firstDayOfMonth];
    int weekday = [comps weekday];
//      NSLog(@"components%d %d %d",_selectedDate,_selectedMonth,_selectedYear);
    weekday  = weekday - 2;
    
    if(weekday < 0)
        weekday += 7;
    
    NSCalendar *c = [NSCalendar currentCalendar];
    NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:self.calendarDate];
    
   
    NSInteger monthLength = days.length;
    
    UILabel *titleText = [[UILabel alloc]initWithFrame:CGRectMake(0,20, self.bounds.size.width, 40)];
    titleText.backgroundColor=[UIColor clearColor];
    titleText.textAlignment = NSTextAlignmentCenter;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMMM yyyy"];
    NSString *dateString = [[format stringFromDate:self.calendarDate] uppercaseString];
    [titleText setText:dateString];
    [titleText setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15.0f]];
    [titleText setTextColor:[UIColor blackColor]];
    [self addSubview:titleText];
    
    //new code
    greenview.frame=CGRectMake(originX, originY+5, width*7, height);
    greenview.backgroundColor=[UIColor greenColor];
    [self addSubview:greenview];
    for (int i =0; i<_weekNames.count; i++)
    {
        UIButton *weekNameLabel = [UIButton buttonWithType:UIButtonTypeCustom];
        weekNameLabel.titleLabel.text = [_weekNames objectAtIndex:i];
        [weekNameLabel setTitle:[_weekNames objectAtIndex:i] forState:UIControlStateNormal];
        [weekNameLabel setFrame:CGRectMake(originX+(width*(i%columns)), originY+5, width, height)];
        [weekNameLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [weekNameLabel.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
        weekNameLabel.userInteractionEnabled = NO;
        [self addSubview:weekNameLabel];
    }
    for (NSInteger i= 0; i<monthLength; i++)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i+1;
        button.titleLabel.text = [NSString stringWithFormat:@"%d",i+1];
        [button setTitle:[NSString stringWithFormat:@"%d",i+1] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.0f]];
        [button addTarget:self action:@selector(tappedDate:) forControlEvents:UIControlEventTouchUpInside];
        NSInteger offsetX = (width*((i+weekday)%columns));
        NSInteger offsetY = (width *((i+weekday)/columns));
        [button setFrame:CGRectMake(originX+offsetX, originY+40+offsetY, width, width)];
        [button.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [button.layer setBorderWidth:1.0];
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = [UIColor lightGrayColor];
        if(((i+weekday)/columns)==0)
        {
            [lineView setFrame:CGRectMake(0, 0, button.frame.size.width, 4)];
            [button addSubview:lineView];
        }

        if(((i+weekday)/columns)==((monthLength+weekday-1)/columns))
        {
            [lineView setFrame:CGRectMake(0, button.frame.size.width-4, button.frame.size.width, 4)];
            [button addSubview:lineView];
        }
        
        UIView *columnView = [[UIView alloc]init];
        [columnView setBackgroundColor:[UIColor lightGrayColor]];
        if((i+weekday)%7==0)
        {
            [columnView setFrame:CGRectMake(0, 0, 4, button.frame.size.width)];
            [button addSubview:columnView];
        }
        else if((i+weekday)%7==6)
        {
            [columnView setFrame:CGRectMake(button.frame.size.width-4, 0, 4, button.frame.size.width)];
            [button addSubview:columnView];
        }
        
        if(i+1 ==_todaydate && components.month == _todaymonth && components.year == _todayyear)
        {
            [button setBackgroundColor:[UIColor lightGrayColor]];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
        }
#pragma new code placed here to schedule array
       
//        _selectedDate=10;
//        _selectedMonth=3;
//        _selectedYear=2015;
        
        UIImageView *insideimage=[[UIImageView alloc] init];
        
        // new code
        if([scheduleddate count]>0&&scheduleddate)
        {
        
        for (int k=0;k<[datearray count]; k++)
        {
            if(i+1 ==[[datearray objectAtIndex:k] integerValue] && components.month == [[montharray objectAtIndex:k] integerValue] && components.year == [[yeararray objectAtIndex:k] integerValue])
            {
                
                if(app->noschedulefound!=TRUE){
                    app->noschedulefound=TRUE;
                }
                
               // NSLog(@"app->noschedulefound %hhd",app->noschedulefound);
                
               // [button setBackgroundColor:[UIColor blueColor]];
                if(_todaydate ==[[datearray objectAtIndex:k] integerValue] && _todaymonth == [[montharray objectAtIndex:k] integerValue] && _todayyear == [[yeararray objectAtIndex:k] integerValue])
                {
                    
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [button setBackgroundColor:[UIColor lightGrayColor]];
                insideimage.frame=CGRectMake(5, 5, button.frame.size.width-10, button.frame.size.height-10);
                    insideimage.clipsToBounds=YES;
                    insideimage.layer.cornerRadius=insideimage.frame.size.width/2;
               // insideimage.image=[UIImage imageNamed:@"whitedot1.png"];
                 //   insideimage.image=[UIImage imageNamed:@"selected_date_Tile.png"];
                    
                    if ([[getsessionStatusArr objectAtIndex:k]isEqualToString:@"Held"]) {
                        
                        
                          insideimage.backgroundColor=[UIColor greenColor];
                        
                    }else{
                        
                          insideimage.backgroundColor=[UIColor redColor];
                        
                        
                    }
            
                   // insideimage.backgroundColor=[UIColor greenColor];
          
                [button addSubview:insideimage];
                    [button sendSubviewToBack:insideimage];
                button.layer.borderWidth=1.0;
                button.layer.borderColor=[UIColor blueColor].CGColor;
                }
                else
                {
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    insideimage.frame=CGRectMake(5, 5, button.frame.size.width-10, button.frame.size.height-10);
                    insideimage.clipsToBounds=YES;
                    insideimage.layer.cornerRadius=insideimage.frame.size.width/2;
                    
               
                    
                    if ([[getsessionStatusArr objectAtIndex:k]isEqualToString:@"Held"]) {
                        
                        
                        insideimage.backgroundColor=[UIColor greenColor];
                        
                    }else{
                        
                        insideimage.backgroundColor=[UIColor redColor];
                        
                        
                    }
                [button addSubview:insideimage];
                     [button sendSubviewToBack:insideimage];
                }
                [self.delegate getmonthDetailon_Swipe:app->noschedulefound];
                }
            else
            {
               // NSLog(@"else condition");
                if(app->noschedulefound!=TRUE){
                    app->noschedulefound=FALSE;
                    [self.delegate getmonthDetailon_Swipe:app->noschedulefound];
                }
                
            }
           
        }
           
        }
        // here can write code
        [self addSubview:button];
        
    }
    
    NSDateComponents *previousMonthComponents = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    previousMonthComponents.month -=1;
    NSDate *previousMonthDate = [gregorian dateFromComponents:previousMonthComponents];
    NSRange previousMonthDays = [c rangeOfUnit:NSDayCalendarUnit
                   inUnit:NSMonthCalendarUnit
                  forDate:previousMonthDate];
    NSInteger maxDate = previousMonthDays.length - weekday;
    
    
    for (int i=0; i<weekday; i++)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.titleLabel.text = [NSString stringWithFormat:@"%d",maxDate+i+1];
        [button setTitle:[NSString stringWithFormat:@"%d",maxDate+i+1] forState:UIControlStateNormal];
        NSInteger offsetX = (width*(i%columns));
        NSInteger offsetY = (width *(i/columns));
        [button setFrame:CGRectMake(originX+offsetX, originY+40+offsetY, width, width)];
        [button.layer setBorderWidth:1.0];
        // for border
        [button.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
        UIView *columnView = [[UIView alloc]init];
        [columnView setBackgroundColor:[UIColor lightGrayColor]];
        if(i==0)
        {
            [columnView setFrame:CGRectMake(0, 0, 4, button.frame.size.width)];
            [button addSubview:columnView];
        }

        UIView *lineView = [[UIView alloc]init];
        // for border
        [lineView setBackgroundColor:[UIColor lightGrayColor]];
        [lineView setFrame:CGRectMake(0, 0, button.frame.size.width, 4)];
        [button addSubview:lineView];
        [button setTitleColor:[UIColor colorWithRed:229.0/255.0 green:231.0/255.0 blue:233.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.0f]];
        [button setEnabled:NO];
        [self addSubview:button];
    }
    
    NSInteger remainingDays = (monthLength + weekday) % columns;
    if(remainingDays >0)
    {
        for (int i=remainingDays; i<columns; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.titleLabel.text = [NSString stringWithFormat:@"%d",(i+1)-remainingDays];
            [button setTitle:[NSString stringWithFormat:@"%d",(i+1)-remainingDays] forState:UIControlStateNormal];
            NSInteger offsetX = (width*((i) %columns));
            NSInteger offsetY = (width *((monthLength+weekday)/columns));
            [button setFrame:CGRectMake(originX+offsetX, originY+40+offsetY, width, width)];
            [button.layer setBorderWidth:1.0];
            [button.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
            UIView *columnView = [[UIView alloc]init];
            [columnView setBackgroundColor:[UIColor lightGrayColor]];
            if(i==columns - 1)
            {
                [columnView setFrame:CGRectMake(button.frame.size.width-4, 0, 4, button.frame.size.width)];
                [button addSubview:columnView];
            }
            UIView *lineView = [[UIView alloc]init];
            [lineView setBackgroundColor:[UIColor lightGrayColor]];
            [lineView setFrame:CGRectMake(0, button.frame.size.width-4, button.frame.size.width,4)];
            [button addSubview:lineView];
            [button setTitleColor:[UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1.0] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.0f]];
            [button setEnabled:NO];
            [self addSubview:button];

        }
    }
//    if(loadermaintain)
//    {
//    [SVProgressHUD dismiss];
//    loadermaintain=FALSE;
//    }
    
   
}
-(IBAction)tappedDate:(UIButton *)sender
{
    
    //NSDayCalendarUnit
    gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:self.calendarDate];
//    if(!(_selectedDate == sender.tag && _selectedMonth == [components month] && _selectedYear == [components year]))
//    {
        if(_selectedDate != -1)
        {
            UIButton *previousSelected =(UIButton *) [self viewWithTag:_selectedDate];
            [previousSelected setBackgroundColor:[UIColor clearColor]];
//            if(previousSelected.tag==_todaydate)
//            {
//              // [previousSelected setBackgroundColor:[UIColor lightGrayColor]];
//                [previousSelected setBackgroundColor:[UIColor clearColor]];
//            }
//            else
//            {
//            [previousSelected setBackgroundColor:[UIColor clearColor]];
//            }
            [previousSelected setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        
        [sender setHighlighted:YES];
        [sender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _selectedDate = sender.tag;
       // NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:self.calendarDate];
        components.day = _selectedDate;
        _selectedMonth = components.month;//NSDayCalendarUnit
        _selectedYear = components.year;
        NSDate *clickedDate = [gregorian dateFromComponents:components];
        [self.delegate tappedOnDate:clickedDate];
   // }
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    components.day = 1;
    components.month += 1;
    self.calendarDate = [gregorian dateFromComponents:components];
    [UIView transitionWithView:self
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
    {
                        [self setNeedsDisplay];
        
        
                    }
                    completion:nil];
    
  
    
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    components.day = 1;
    components.month -= 1;
    self.calendarDate = [gregorian dateFromComponents:components];
    [UIView transitionWithView:self
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^ {
                        [self setNeedsDisplay];
                     }
                    completion:nil];
    
    
}
-(void)setCalendarParameters
{
    if(gregorian == nil)
    {
        gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
        _selectedDate  = components.day;
        _selectedMonth = components.month;
        _selectedYear = components.year;
        
        // new code
        _todaydate=components.day;
        _todaymonth = components.month;
        _todayyear = components.year;
    }
}

@end
