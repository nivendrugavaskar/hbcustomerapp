//
//  ChangePasswordVCViewController.h
//  HealthyBillionCustomer
//
//  Created by Arnab on 26/03/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SWRevealViewController.h"

@interface ChangePasswordVCViewController : UIViewController<UITextFieldDelegate,SWRevealViewControllerDelegate>{
    
    AppDelegate *app;

    
}

@property (weak, nonatomic) IBOutlet UITextField *txtFld_OldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_NewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_ReEnterNPassword;
- (IBAction)btnAction_Save:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *activetextfield;

@end
