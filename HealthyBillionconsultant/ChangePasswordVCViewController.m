//
//  ChangePasswordVCViewController.m
//  HealthyBillionCustomer
//
//  Created by Arnab on 26/03/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "ChangePasswordVCViewController.h"

@interface ChangePasswordVCViewController (){
    
    BOOL editingmode;
    UIView *changepwdview;
    UIView *paddingView;
    CGRect screenBounds;
    CGPoint pointtrack,pointtrackcontentoffset;
    UIImageView *buttonimageview;


}

@end

@implementation ChangePasswordVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
      app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtFld_OldPassword.leftView = paddingView;
    _txtFld_OldPassword.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtFld_NewPassword.leftView = paddingView;
    _txtFld_NewPassword.leftViewMode = UITextFieldViewModeAlways;
    
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtFld_ReEnterNPassword.leftView = paddingView;
    _txtFld_ReEnterNPassword.leftViewMode = UITextFieldViewModeAlways;
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    screenBounds=[[UIScreen mainScreen] bounds];
    pointtrackcontentoffset=self.view.frame.origin;

    
    [super viewWillAppear:animated];
    self.title = @"CHANGE PASSWORD";
    [self setCustomNavigationButton];
}
#pragma mark -- TEXTFIELDDELEGATEMETHOD
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 1000;
    toolBar.barTintColor=[UIColor colorWithRed:30/255.0f green:87/255.0f blue:140/255.0f alpha:1.0f];
    toolBar.tintColor=[UIColor whiteColor];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismisskeyboard)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    
    
    [_txtFld_NewPassword setInputAccessoryView:toolBar];
    
    [_txtFld_OldPassword setInputAccessoryView:toolBar];
    
    [_txtFld_ReEnterNPassword setInputAccessoryView:toolBar];
    
    
    self.activetextfield=textField;
    pointtrack = textField.frame.origin;
    
    CGRect rect=self.view.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
   if(screenBounds.size.height==480)
    {
        if(pointtrack.y>100)
        {
       rect.origin.y-=pointtrack.y-20;
        }
    
    }
    self.view.frame=rect;
    [UIView commitAnimations];
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if([textField tag] == 12)
    {
        [_txtFld_NewPassword becomeFirstResponder];
    }else if([textField tag]==13){
        
        [_txtFld_ReEnterNPassword becomeFirstResponder];
    }else
    {
        [self dismisskeyboard];
        
    }
    return NO;
    
}
-(void)dismisskeyboard
{
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
    [self.activetextfield resignFirstResponder];
}
#pragma cutomize navigation
-(void)setCustomNavigationButton
{
    
    [self.navigationController.navigationBar setCustomNavigationButton:self];
    
       self.revealViewController.delegate=self;
    
}
#pragma swrevealview delegate
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    [self dismisskeyboard];
}

-(BOOL)shouldAutorotate{
    
    return NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnAction_Save:(id)sender
{
    
    
          if(![NSString validation:_txtFld_OldPassword.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill old password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
          //else if (![_txtFld_OldPassword.text isEqualToString:app->customerpwd]){
//            
//            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please enter correct old password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//            return;
//            
//        }
        else if(![NSString validation:_txtFld_NewPassword.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill new password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        else if(![NSString validation:_txtFld_ReEnterNPassword.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill re-enter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        else if(![_txtFld_OldPassword.text isEqualToString:app->customerpwd])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill correct old password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            _txtFld_NewPassword.text=@"";
            return;
        }
        else if(![_txtFld_NewPassword.text isEqualToString:_txtFld_ReEnterNPassword.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill correct re-enter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            _txtFld_ReEnterNPassword.text=@"";
            [alert show];
            return;
        }
        else
        {
            
            [self dismisskeyboard];
            [self changePassword];
      
        }

    
}
#pragma Webservice for sucessful password set
-(void)changePassword
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Updating.."];
  
    NSString *path =[NSString stringWithFormat:@"%@changePassword",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",[NSString stringWithFormat:@"19x%@",[app->globalcustomerdetail valueForKeyPath:@"data.Account.userid"]],@"id",_txtFld_OldPassword.text,@"oldPassword",_txtFld_NewPassword.text,@"newPassword",_txtFld_ReEnterNPassword.text,@"confirmPassword",nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
  
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     
     
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
#pragma store customer value in global dictionary
       //  app->globalcustomerdetail=[dict valueForKey:@"result"];
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
             
         }
         else
         {
             
             app->customerpwd=_txtFld_NewPassword.text;
             NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
             [def setValue:app->customerpwd forKey:@"customerpassword"];
             [def synchronize];
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Password Changed Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             _txtFld_OldPassword.text=@"";
             _txtFld_NewPassword.text=@"";
             _txtFld_ReEnterNPassword.text=@"";
             
            }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
#pragma send to dash board
       //  [self performSegueWithIdentifier:@"segueToHome" sender:self];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}


@end
