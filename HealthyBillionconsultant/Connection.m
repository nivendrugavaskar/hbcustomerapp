//
//  Connection.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 25/02/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "Connection.h"
#import "AppDelegate.h"

 AppDelegate *app;


@implementation Connection

-(void)sessionfetch
{
    
   // app=(AppDelegate *)[UIApplication sharedApplication].delegate;
   
    NSString *myString = [NSString stringWithFormat:@"%@%@",app->customertoken,app->customeraccesskey];
    NSString *md5 = [NSString md5HexDigest:myString];
    app->MD5string=md5;
    NSString *path =[NSString stringWithFormat:@"%@login",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customerusername,@"username",md5,@"accessKey",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
       //  NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             
             //[[dict valueForKey:@"error"] valueForKey:@"message"]
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Error" message:@"Invalid access key" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
         }
         else
         {
             app->customersessionname=[[dict valueForKey:@"result"] valueForKey:@"sessionName"];
//             if(app->userdefaultcheck)
//             {
             [self userdetailservice];
//             }
//             else
//             {
//               [self.connectionDelegate sucessdone:dict];   
//             }
             
         }
        // [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        // [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
        
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
     }];
    
}
-(void)userdetailservice
{
    
    
    NSString *path =[NSString stringWithFormat:@"%@getUserInfo",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    //[SVProgressHUD showWithStatus:@"Please wait.."];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
        // NSLog(@"%@",dict);

         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Error" message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
             
         }
         else
         {
#pragma store customer value in global dictionary
             app->globalcustomerdetail=[dict valueForKey:@"result"];
            // app->consultantuserid=[NSString stringWithFormat:@"19x%@",[app->globalcustomerdetail valueForKeyPath:@"data.User.id"]];
             if([[app->globalcustomerdetail valueForKeyPath:@"data.User.first_time_reg"] integerValue]==0)
             {
             app->registerdfirst=FALSE;
             }
             else
             {
             app->registerdfirst=TRUE;
             }
#pragma store username in user default
             if(app->userdefaultcheck)
             {
             NSUserDefaults *default1=[NSUserDefaults standardUserDefaults];
             [default1 setValue:app->customerusername forKey:@"customerusername"];
             [default1 setValue:app->customertoken forKey:@"customertoken"];
             [default1 setValue:app->customersessionname forKey:@"customersession"];
             [default1 setValue:app->customeraccesskey forKey:@"customeraccesskey"];
             [default1 setValue:app->MD5string forKey:@"MD5key"];
            // [default1 setValue:app->consultantuserid forKey:@"consultantuserid"];
             [default1 setBool:app->registerdfirst forKey:@"registered"];
             [default1 synchronize];
             }
            
         }
        // [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        // [SVProgressHUD dismiss];
#pragma send to dash board
         [self.connectionDelegate sucessdone:dict];
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}

#pragma token
-(void)tokenfetch
{
    
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Loading...."];
    NSString *path =[NSString stringWithFormat:@"%@getchallenge",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customerusername,@"username",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
        // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Error" message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             
         }
         else
         {
             app->customertoken=[[dict valueForKey:@"result"] valueForKey:@"token"];
             [self sessionfetch];
             // [self performSegueWithIdentifier:@"pushTopassword" sender:self];
         }
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
     }];
    
}

@end