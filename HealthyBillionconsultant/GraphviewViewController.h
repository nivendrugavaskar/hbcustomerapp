//
//  GraphviewViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru Gavaskar on 25/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "customdatepicker.h"

@interface GraphviewViewController : UIViewController
{
    AppDelegate *app;
    //UIDatePicker *datePicker;
}
- (IBAction)back:(id)sender;
- (IBAction)didtapprevious:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnprevious;
- (IBAction)didtapnext:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnnext;
@property (strong, nonatomic) IBOutlet UILabel *ratinglabel;
@property (strong, nonatomic) IBOutlet UILabel *dayslabel;
@property (strong, nonatomic) IBOutlet UILabel *headinglabel;

@property (strong,nonatomic) NSMutableArray *graphdataarray;
- (IBAction)didTapselectmonth:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnselectmonth;
@property (strong, nonatomic) IBOutlet UILabel *nodatamonthlabel;

@end
