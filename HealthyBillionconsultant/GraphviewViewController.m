//
//  GraphviewViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru Gavaskar on 25/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "GraphviewViewController.h"

@interface GraphviewViewController ()
{
    SHLineGraphView *_lineGraph;
    SHPlot *_plot1;
    NSDictionary *dict;
    NSInteger pagenumber,initialpage;
    BOOL enddata,enterfirst,activitytest;
    NSMutableArray *salesordertrackarray,*customerperformancearray,*montharray,*yeararray;
    UIActivityIndicatorView *activityIndicator;
    NSString *monthtitle,*yeartitle;
    
    customdatepicker *datePicker;
}

@end

@implementation GraphviewViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    NSLog(@"%f",self.view.frame.size.height);
    enterfirst=TRUE;
    salesordertrackarray=[[NSMutableArray alloc] init];
    customerperformancearray=[[NSMutableArray alloc] init];
    _ratinglabel.transform=CGAffineTransformMakeRotation(- ( 90 * M_PI ) / 180 );
    _ratinglabel.frame=CGRectMake(0, _ratinglabel.frame.origin.y, _ratinglabel.frame.size.width, _ratinglabel.frame.size.height);
    _dayslabel.frame=CGRectMake(_dayslabel.frame.origin.x, _dayslabel.frame.origin.y, _dayslabel.frame.size.width, _dayslabel.frame.size.height);
    _dayslabel.text=@"No. of Days-->";
    _ratinglabel.hidden=YES;
    _dayslabel.hidden=YES;
    _nodatamonthlabel.hidden=YES;
    // call service here
    
    NSLog(@"%@",_graphdataarray);
    // new code
    NSDate *today = [[NSDate alloc] init];
    NSCalendar *calendar1 = [NSCalendar currentCalendar];
    NSDateComponents *comps1 = [[NSDateComponents alloc] init];
    comps1= [calendar1 components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    NSInteger year=comps1.year;
    NSInteger month=comps1.month;
    
    montharray=[[NSMutableArray alloc]initWithObjects:@"January",@"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December", nil];
    yeararray=[[NSMutableArray alloc]init];
    for (int i=2014; i<=year; i++)
    {
        [yeararray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    monthtitle=[montharray objectAtIndex:month-1];
    yeartitle=[NSString stringWithFormat:@"%d",year];
    [_btnselectmonth setTitle:[NSString stringWithFormat:@"%@,%@",monthtitle,yeartitle] forState:UIControlStateNormal];
    
   // NSInteger anIndex=[montharray indexOfObject:yeartitle];
    activitytest=FALSE;
    [self servicegraphdata:[NSString stringWithFormat:@"%d",month]:yeartitle];
    
 
}
-(void)servicegraphdata:(NSString *)month :(NSString *)year
{
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.color=[UIColor redColor];
    if(!activitytest)
    {
    activityIndicator.center = CGPointMake(CGRectGetMidY(self.view.bounds),CGRectGetMidX(self.view.bounds));
    }
    else
    {
     activityIndicator.center=self.view.center;
    }
    activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    // end
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    NSString *path=[NSString stringWithFormat:@"%@getCustomerProgressData",app->parentUrl];
    NSDictionary *params;
    
    params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",[app->globalcustomerdetail valueForKeyPath:@"data.Account.accountid"],@"accountid",[_graphdataarray valueForKey:@"taskid"],@"taskid",month,@"month",year,@"year",nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         dict=[[NSDictionary alloc] init];
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             _nodatamonthlabel.hidden=YES;
             
         }
         else
         {
             customerperformancearray=[[NSMutableArray alloc] init];
             if([dict valueForKey:@"result"]==nil||[[dict valueForKey:@"result"] isKindOfClass:[NSNull class]])
             {
                 if([customerperformancearray count]==0)
                 {
                     [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                     _headinglabel.text=[_graphdataarray valueForKey:@"tasksname"];
                     //   app->hudmaintain=FALSE;
                     [activityIndicator removeFromSuperview];
                     _dayslabel.hidden=YES;
                     _ratinglabel.hidden=YES;
                     _nodatamonthlabel.hidden=NO;
                     
                     return;
                 }
                 pagenumber--;
                 enddata=TRUE;
             }
             else
             {
                 enddata=FALSE;
                 customerperformancearray=[[dict valueForKey:@"result"] mutableCopy];
                 
             }
             // again here call bydefauly 0 index
             if([customerperformancearray count]>0)
             {
                 initialpage=0;
                 _ratinglabel.hidden=NO;
                 _dayslabel.hidden=NO;
                 
                 // new code
                 //new code
                  [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 _headinglabel.text=[_graphdataarray valueForKey:@"tasksname"];
                 _nodatamonthlabel.hidden=YES;
                 //   app->hudmaintain=FALSE;
                 [activityIndicator removeFromSuperview];
                 
                 
                 // initialize
                 //initate the graph view
                 if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
                 {
                     _lineGraph = [[SHLineGraphView alloc] initWithFrame:CGRectMake(10,95, self.view.frame.size.width-15, self.view.frame.size.height-120)];
                 }
                 else
                 {
                     _lineGraph = [[SHLineGraphView alloc] initWithFrame:CGRectMake(10,95, self.view.frame.size.height-15, self.view.frame.size.width-120)];
                 }
                 
                 
                 
                 //create a new plot object that you want to draw on the `_lineGraph`
                 _plot1 = [[SHPlot alloc] init];
                 //set the main graph area theme attributes
                 
                 /**
                  *  theme attributes dictionary. you can specify graph theme releated attributes in this dictionary. if this property is
                  *  nil, then a default theme setting is applied to the graph.
                  */
                 NSDictionary *_themeAttributes = @{
                                                    kXAxisLabelColorKey : [UIColor colorWithRed:0.48 green:0.48 blue:0.49 alpha:0.4],
                                                    kXAxisLabelFontKey : [UIFont fontWithName:@"TrebuchetMS" size:10],
                                                    kYAxisLabelColorKey : [UIColor colorWithRed:0.48 green:0.48 blue:0.49 alpha:0.4],
                                                    kYAxisLabelFontKey : [UIFont fontWithName:@"TrebuchetMS" size:10],
                                                    kYAxisLabelSideMarginsKey : @20,
                                                    kPlotBackgroundLineColorKey : [UIColor colorWithRed:0.48 green:0.48 blue:0.49 alpha:0.4],
                                                    kDotSizeKey : @10
                                                    };
                 _lineGraph.themeAttributes = _themeAttributes;
                 
                 //set the line graph attributes
                 
                 /**
                  *  the maximum y-value possible in the graph. make sure that the y-value is not in the plotting points is not greater
                  *  then this number. otherwise the graph plotting will show wrong results.
                  */
                 _lineGraph.yAxisRange = @(5);
                 
                 /**
                  *  y-axis values are calculated according to the yAxisRange passed. so you do not have to pass the explicit labels for
                  *  y-axis, but if you want to put any suffix to the calculated y-values, you can mention it here (e.g. K, M, Kg ...)
                  */
                 _lineGraph.yAxisSuffix = @"";
                 
                 _lineGraph.xAxisValues=[[NSMutableArray alloc] init];
                 _plot1.plottingValues=[[NSMutableArray alloc] init];
                 
                 
                 NSMutableArray *arr=[[NSMutableArray alloc] init];
                 
                 // Set up dictionary
                 if([customerperformancearray count]>0)
                 {
                     for(int i=0;i<[customerperformancearray count];i++)
                     {
                         NSMutableDictionary *localdict=[[NSMutableDictionary alloc] init];
                         [localdict setValue:[NSString stringWithFormat:@"%d",i+1] forKey:[NSString stringWithFormat:@"%d",i+1]];
                         
                         [_lineGraph.xAxisValues addObject:localdict];
                         
                         localdict=[[NSMutableDictionary alloc] init];
                         [localdict setValue:[NSString stringWithFormat:@"%@",[[customerperformancearray objectAtIndex:i] valueForKey:@"ratings"]] forKey:[NSString stringWithFormat:@"%d",i+1]];
                         
                         [_plot1.plottingValues addObject:localdict];
                         
                         [arr addObject:[NSString stringWithFormat:@"%@",[[customerperformancearray objectAtIndex:i] valueForKey:@"ratings"]]];
                         
                         
                     }
                 }
                 NSLog(@"%@",_plot1.plottingValues);
                 
                 _plot1.plottingPointsLabels = [[NSMutableArray alloc] init];
                 _plot1.plottingPointsLabels=arr;
                 
                 NSDictionary *_plotThemeAttributes = @{
                                                        kPlotFillColorKey : [UIColor colorWithRed:0.47 green:0.75 blue:0.78 alpha:0.5],
                                                        kPlotStrokeWidthKey : @2,
                                                        kPlotStrokeColorKey : [UIColor colorWithRed:0.18 green:0.36 blue:0.41 alpha:1],
                                                        kPlotPointFillColorKey : [UIColor colorWithRed:0.18 green:0.36 blue:0.41 alpha:1],
                                                        kPlotPointValueFontKey : [UIFont fontWithName:@"TrebuchetMS" size:18]
                                                        };
                 
                 _plot1.plotThemeAttributes = _plotThemeAttributes;
                 [_lineGraph addPlot:_plot1];
                 //You can as much `SHPlots` as you can in a `SHLineGraphView`
                 [_lineGraph setupTheView];
                 [self.view addSubview:_lineGraph];
                 
                 // end here
             }
             else
             {
                 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 _headinglabel.text=[_graphdataarray valueForKey:@"tasksname"];
                 //   app->hudmaintain=FALSE;
                 [activityIndicator removeFromSuperview];
                 
             }
             
         }
         
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [self setNeedsStatusBarAppearanceUpdate];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


/*-(NSUInteger)supportedInterfaceOrientations
{
    if(isorientation)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
         return UIInterfaceOrientationMaskLandscapeRight|UIInterfaceOrientationLandscapeLeft;
    }
    return UIInterfaceOrientationMaskPortrait;
}
*/
- (IBAction)back:(id)sender
{
    isorientation=FALSE;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didtapprevious:(id)sender
{
    activitytest=TRUE;
    [_lineGraph removeFromSuperview];
    _ratinglabel.hidden=YES;
    _dayslabel.hidden=YES;
    
    // new code
    NSDate *today = [[NSDate alloc] init];
    NSCalendar *calendar1 = [NSCalendar currentCalendar];
    NSDateComponents *comps1 = [[NSDateComponents alloc] init];
    comps1= [calendar1 components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    NSInteger anIndex=[montharray indexOfObject:monthtitle];
    if(anIndex==0)
    {
        NSString *startyear=[NSString stringWithFormat:@"%d",[yeartitle integerValue]-1];
        if([startyear isEqualToString:@"2013"])
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Can't select year less than 2014" message:@"Please select year greater than 2014" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        yeartitle=[NSString stringWithFormat:@"%d",[yeartitle integerValue]-1];
        anIndex=12;
    }
   
    
    monthtitle=[montharray objectAtIndex:anIndex-1];
    [_btnselectmonth setTitle:[NSString stringWithFormat:@"%@,%@",monthtitle,yeartitle] forState:UIControlStateNormal];
    [self servicegraphdata:[NSString stringWithFormat:@"%d",anIndex] :yeartitle];
   
   
}
- (IBAction)didtapnext:(id)sender
{
    activitytest=TRUE;
    
    [_lineGraph removeFromSuperview];
    _ratinglabel.hidden=YES;
    _dayslabel.hidden=YES;
    
    // new code
    NSDate *today = [[NSDate alloc] init];
    NSCalendar *calendar1 = [NSCalendar currentCalendar];
    NSDateComponents *comps1 = [[NSDateComponents alloc] init];
    comps1= [calendar1 components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    NSInteger currentyear=comps1.year;
    NSInteger currentmonth=comps1.month;
    
    NSInteger anIndex=[montharray indexOfObject:monthtitle];
    if(anIndex==11)
    {
        yeartitle=[NSString stringWithFormat:@"%d",[yeartitle integerValue]+1];
        anIndex=-1;
    }
    
    if(((anIndex+2)>currentmonth)&&([yeartitle intValue]==currentyear))
    {
        _nodatamonthlabel.hidden=YES;
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Can't select future month and year" message:@"Please select month and year previous than current month and year" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else
    {
        monthtitle = [montharray objectAtIndex:anIndex+1];
        [_btnselectmonth setTitle:[NSString stringWithFormat:@"%@,%@",monthtitle,yeartitle] forState:UIControlStateNormal];
        [self servicegraphdata:[NSString stringWithFormat:@"%d",anIndex+2] :yeartitle];
    }
   
    
}
- (BOOL)shouldAutorotate
{
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        if(enterfirst)
        {
            isorientation=TRUE;
        }
        enterfirst=FALSE;
        return NO;
    }
    else
    {
        isorientation=TRUE;
        return YES;
    }
    
    return NO;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
         toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        return YES;
        }
   return NO;
}

- (IBAction)didTapselectmonth:(id)sender
{
    _btnselectmonth.enabled=FALSE;
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, self.view.bounds.size.width, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, self.view.bounds.size.width, 216);
    CGRect frame=CGRectMake(0, self.view.bounds.size.height+44,self.view.bounds.size.width, 216);
    datePicker = [[customdatepicker alloc] initWithFrame:frame];
    datePicker.backgroundColor=[UIColor whiteColor];
    datePicker.tag=10;
   // datePicker.datePickerMode=UIDatePickerModeDate;
    NSInteger anIndex=[montharray indexOfObject:monthtitle];
    NSInteger yearIndex=[yeararray indexOfObject:yeartitle];
    
    [datePicker selectRow:anIndex inComponent:0 animated:YES];
    [datePicker selectRow:yearIndex inComponent:1 animated:YES];
    
    [self.view addSubview:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    UIBarButtonItem *Cancel=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissDatePicker:)];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(changeDate1:)];
    [toolBar setItems:[NSArray arrayWithObjects:Cancel,spacer, doneButton, nil]];
    
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    [UIView commitAnimations];
}

// new code
- (void)changeDate1:(UIBarButtonItem *)sender
{
     activitytest=TRUE;
    
   // selectedstartdate=datePicker.date;
    NSInteger monthnumber=[datePicker selectedRowInComponent:0];
    NSString *yearvalue =  [datePicker pickerView:datePicker titleForRow:[datePicker selectedRowInComponent:1] forComponent:1];
    [self dismissDatePicker:nil];
    [_lineGraph removeFromSuperview];
    // new code
    NSDate *today = [[NSDate alloc] init];
    NSCalendar *calendar1 = [NSCalendar currentCalendar];
    NSDateComponents *comps1 = [[NSDateComponents alloc] init];
    comps1= [calendar1 components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
    NSInteger currentyear=comps1.year;
    NSInteger currentmonth=comps1.month;
    if(((monthnumber+1)>currentmonth)&&([yearvalue intValue]==currentyear))
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Can't select future month and year" message:@"Please select month and year previous than current month and year" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    monthtitle = [datePicker pickerView:datePicker titleForRow:[datePicker selectedRowInComponent:0] forComponent:0] ;
    yeartitle =  [datePicker pickerView:datePicker titleForRow:[datePicker selectedRowInComponent:1] forComponent:1];
    [_btnselectmonth setTitle:[NSString stringWithFormat:@"%@,%@",monthtitle,yeartitle] forState:UIControlStateNormal];
    
    [self servicegraphdata:[NSString stringWithFormat:@"%d",monthnumber+1] :yeartitle];
    
}

- (void)removeViews:(id)object
{
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
     _btnselectmonth.enabled=TRUE;
}

- (void)dismissDatePicker:(id)sender
{
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, self.view.bounds.size.width, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}
@end
