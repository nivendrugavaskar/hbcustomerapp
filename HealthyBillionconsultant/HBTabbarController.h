//
//  HBTabbarController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 15/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface HBTabbarController : UITabBarController<UIScrollViewDelegate>
{
    
}
@property(nonatomic,strong) IBOutlet UIScrollView *tabBarScroller;

@property (nonatomic,strong)NSMutableArray *sendarrayfromtab;

@property BOOL pendingcollection;
@end
