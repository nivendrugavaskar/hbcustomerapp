//
//  HBTabbarController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 15/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "HBTabbarController.h"

@interface HBTabbarController ()
{
    AppDelegate *app;
    int flag;
}

@end

@implementation HBTabbarController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // self.delegate = self;
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    // hide moreview controller of tabbar
    UINavigationController *moreViewController = self.moreNavigationController;
    
    if(moreViewController)
    {
        moreViewController.navigationBar.hidden = YES;
        
        //[moreViewController setToolbarHidden:YES];
    }
    moreViewController.interactivePopGestureRecognizer.enabled=NO;
    
    [self.navigationController.navigationBar setCustomNavigationButton:self];
    
    // add back button
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    //[backbarbutton setAction:@selector(back)];
    // [backbarbutton setTarget:self];
    backbarbutton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItems = @[backbarbutton];
    
    self.tabBar.hidden = YES;
    
    // costomize tabbar
    
#pragma mark - For Showing Tab Bar
//    _tabBarScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.tabBar.frame.size.width, self.tabBar.frame.size.height-1)];
//    _tabBarScroller.bounces = NO;
//    NSLog(@"self.tabBar.frame.size.height %f",self.tabBar.frame.size.height);
//    [_tabBarScroller setContentSize:CGSizeMake(550, 0)];
//    _tabBarScroller.backgroundColor = [UIColor colorWithRed:77/255.0f green:197/255.0f blue:221/255.0f alpha:1.0];
//    _tabBarScroller.showsHorizontalScrollIndicator = NO;
    
    
#pragma mark - For adding extra tab bar [Self addButtons]
    
   // [self addButtons];
    
#pragma mark -- White Button Add
    
//    UIView *inititalHolder =  [[UIView alloc] initWithFrame:CGRectMake(self.tabBar.frame.origin.x,64, self.tabBar.frame.size.width, self.tabBar.frame.size.height)];
//    inititalHolder.backgroundColor = [UIColor whiteColor];
//    [inititalHolder addSubview:_tabBarScroller];
//    [self.view addSubview:inititalHolder];
    
    // Do any additional setup after loading the view.
}

// back
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    
    NSLog(@"%@",self.sendarrayfromtab);
    if(_pendingcollection)
    {
      [self activeModeTabSelect:2];
    }
    else
    {
    [self activeModeTabSelect:0];
    }
    
    
}



#pragma mark-Add buttons


-(void)addButtons {
    
    for(int i =1;i<=5;i++)
    {
        UIButton *btn= [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake((i-1)*110, 0, 110, self.tabBar.frame.size.height-5);
        
        //rgb(77, 197, 221)
        
        // btn.backgroundColor = [UIColor colorWithRed:77/255.0f green:197/255.0f blue:221/255.0f alpha:1.0];
        
        btn.titleLabel.font = [UIFont fontWithName:@"Arial" size:12];
        btn.titleLabel.textColor = [UIColor whiteColor];
        btn.tag = i-1;
        
        
        UILabel *tabName = [[UILabel alloc] initWithFrame:CGRectMake((i-1)*110, (self.tabBar.frame.size.height/2)-6, 110, 12)];
        tabName.font = [UIFont systemFontOfSize:9];
        tabName.textAlignment = NSTextAlignmentCenter;
        tabName.tag = i-1;
        
        
        UIImageView *imgvw = [[UIImageView alloc] initWithFrame:CGRectMake((i-1)*110,(self.tabBar.frame.size.height-5), 110, 5)];
        imgvw.tag = i-1;
        
        switch (btn.tag) {
            case 0:
                tabName.text = @"CONS";
                // [btn setImage:[UIImage imageNamed:@"contact_list.png"] forState:UIControlStateNormal];
                
                break;
            case 1:
                tabName.text = @"PAID SESSION";
                // [btn setImage:[UIImage imageNamed:@"recent_chat.png"] forState:UIControlStateNormal];
                break;
            case 2:
                tabName.text = @"UNPAID SESSION";
                // [btn setImage:[UIImage imageNamed:@"gallery"] forState:UIControlStateNormal];
                break;
                
            case 3:
                tabName.text = @"COMPLETED SESSION";
                // [btn setImage:[UIImage imageNamed:@"my_profile.png"] forState:UIControlStateNormal];
                break;
                
            case 4:
                tabName.text = @"FEEDBACK";
                // [btn setImage:[UIImage imageNamed:@"my_profile.png"] forState:UIControlStateNormal];
                break;
                
                
                
            default:
                break;
        }
        
        [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [_tabBarScroller addSubview:tabName];
        [_tabBarScroller addSubview:imgvw];
        [_tabBarScroller addSubview:btn];
    }
}

#pragma mark- Highlight When App is active

-(void)activeModeTabSelect:(int)tag
{
    if(_pendingcollection)
    {
        float x;
        [self setSelectedViewController:[self.viewControllers objectAtIndex:tag]];
        [self deselectOtherButton:tag];
        // new code to set image view red
        for (UIImageView *img in _tabBarScroller.subviews)
        {
            if([img isKindOfClass:[UIImageView class]])
            {
                if(img.tag==tag)
                {
                    img.backgroundColor=[UIColor redColor];
                }
            }
        }
        
        switch (tag)
        {
            case 0:
                self.title=@"CONSULTANT DETAILS";
                [_tabBarScroller setContentOffset:CGPointMake(0, 0) animated:YES];
                break;
            case 1:
                self.title=@"PAID SESSION";
                [_tabBarScroller setContentOffset:CGPointMake(110*(tag-1)+30, 0) animated:YES];
                break;
            case 2:
                self.title=@"UNPAID SESSION";
                [_tabBarScroller setContentOffset:CGPointMake(110*(tag-1)+10, 0) animated:YES];
                break;
            case 3:
                self.title=@"COMPLETED SESSION";
                if(self.view.frame.size.width+120*(tag-1)>_tabBarScroller.contentSize.width)
                {
                    x=(_tabBarScroller.contentSize.width-self.view.frame.size.width);
                }
                else
                {
                    x=120*(tag-1);
                }
                [_tabBarScroller setContentOffset:CGPointMake(x, 0) animated:YES];
                break;
            case 4:
                self.title=@"FEEDBACK";
                if(self.view.frame.size.width+120*(tag-1)>_tabBarScroller.contentSize.width)
                {
                    x=(_tabBarScroller.contentSize.width-self.view.frame.size.width);
                }
                else
                {
                    x=120*(tag-1);
                }
                [_tabBarScroller setContentOffset:CGPointMake(x, 0) animated:YES];
                break;
                
            default:
                break;
        }
    }
    else
    {
    self.title=@"CONSULTANT DETAILS";
    [self setSelectedViewController:[self.viewControllers objectAtIndex:tag]];
    [self deselectOtherButton:tag];
    }

}

#pragma mark- Highlight select image

-(void)buttonTapped:(id)sender
{
    UIButton *trackbutton = (UIButton *)sender;
    float x;
    [self setSelectedViewController:[self.viewControllers objectAtIndex:trackbutton.tag]];
    [self deselectOtherButton:(int)trackbutton.tag];
    // new code to set image view red
    for (UIImageView *img in _tabBarScroller.subviews)
    {
        if([img isKindOfClass:[UIImageView class]])
        {
            if(img.tag==trackbutton.tag)
            {
                img.backgroundColor=[UIColor redColor];
            }
        }
    }
    
    switch (trackbutton.tag)
    {
        case 0:
            self.title=@"CONS";
            [_tabBarScroller setContentOffset:CGPointMake(0, 0) animated:YES];
            break;
        case 1:
            self.title=@"PAID SESSION";
            [_tabBarScroller setContentOffset:CGPointMake(110*(trackbutton.tag-1)+30, 0) animated:YES];
            break;
        case 2:
            self.title=@"UNPAID SESSION";
            [_tabBarScroller setContentOffset:CGPointMake(110*(trackbutton.tag-1)+10, 0) animated:YES];
            break;
        case 3:
            self.title=@"COMPLETED SESSION";
            if(self.view.frame.size.width+120*(trackbutton.tag-1)>_tabBarScroller.contentSize.width)
            {
                x=(_tabBarScroller.contentSize.width-self.view.frame.size.width);
            }
            else
            {
                x=120*(trackbutton.tag-1);
            }
            [_tabBarScroller setContentOffset:CGPointMake(x, 0) animated:YES];
            break;
        case 4:
            self.title=@"FEEDBACK";
            if(self.view.frame.size.width+120*(trackbutton.tag-1)>_tabBarScroller.contentSize.width)
            {
                x=(_tabBarScroller.contentSize.width-self.view.frame.size.width);
            }
            else
            {
                x=120*(trackbutton.tag-1);
            }
            [_tabBarScroller setContentOffset:CGPointMake(x, 0) animated:YES];
            break;
            
        default:
            break;
    }
    
    NSLog(@"flag  %d",flag);
    NSLog(@"Tab bar %ld is clicked",(long)trackbutton.tag);
    
    
}

#pragma mark- deselectOther

-(void)deselectOtherButton:(int )tager
{
    
    NSLog(@"tag %d",tager);
    for (id v in _tabBarScroller.subviews)
    {
        
        UIButton *btn=(UIButton *)v;
        if([btn isKindOfClass:[UIButton class]])
        {
            
            
            if(btn.tag==tager) continue;
            
            switch (btn.tag)
            {
                case 0:
                    
                    // [btn setImage:[UIImage imageNamed:@"contact_list.png"] forState:UIControlStateNormal];
                    break;
                case 1:
                    // [btn setImage:[UIImage imageNamed:@"recent_chat.png"] forState:UIControlStateNormal];
                    break;
                case 2:
                    //  [btn setImage:[UIImage imageNamed:@"gallery"] forState:UIControlStateNormal];
                    break;
                case 3:
                    // [btn setImage:[UIImage imageNamed:@"history"] forState:UIControlStateNormal];
                    break;
                case 4:
                    // [btn setImage:[UIImage imageNamed:@"my_profile.png"] forState:UIControlStateNormal];
                    break;
                    
                default:
                    break;
            }
        }
        
    }
    
    for (id v in _tabBarScroller.subviews)
    {
        
        UIImageView *imgvw=(UIImageView *)v;
        // new code for imageview deselect
        if([imgvw isKindOfClass:[UIImageView class]])
        {
            if(imgvw.tag==tager)
            {
                imgvw.backgroundColor=[UIColor redColor];
                continue;
            }
            switch (imgvw.tag)
            {
                case 0:
                    imgvw.backgroundColor=[UIColor clearColor];
                    break;
                case 1:
                    imgvw.backgroundColor=[UIColor clearColor];
                    break;
                case 2:
                    imgvw.backgroundColor=[UIColor clearColor];
                    break;
                case 3:
                    imgvw.backgroundColor=[UIColor clearColor];
                    break;
                case 4:
                    imgvw.backgroundColor=[UIColor clearColor];
                    break;
                    
                default:
                    break;
            }
        }
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
