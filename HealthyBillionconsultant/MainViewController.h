//
//  MainViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 06/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SWRevealViewController.h"


@protocol RowSelectionDelegate<NSObject>
-(void)selectRowWithHeading:(int)index;
@end

@interface MainViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,SWRevealViewControllerDelegate>
{
    AppDelegate *app;
    UIImagePickerController *ImagePicker;
    UIImage *selectedimage;
    NSData *imageDataPicker;
}

@property (weak, nonatomic) IBOutlet UIView *view_EmailId;

@property (weak, nonatomic) IBOutlet UIView *view_Address;

@property (strong, nonatomic) IBOutlet UIScrollView *aScrollview;
@property (strong, nonatomic) IBOutlet UIImageView *profileImgview;
- (IBAction)didTapimageedit:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectpic;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextView *txtviewAddress;
- (IBAction)didTapChangepwd:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnchangepwd;
@property (strong, nonatomic) IBOutlet UIImageView *photoeditimage;

#pragma custom textfield and text view properties
@property(weak,nonatomic)UITextField *activeTextfield;
@property(weak,nonatomic)UITextView *activeTextview;

@property (strong, nonatomic) IBOutlet UILabel *addresslabel;

@property (retain) id<RowSelectionDelegate> rowSelectionDelegate;
@end
