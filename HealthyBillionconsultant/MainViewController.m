//
//  MainViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 06/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "MainViewController.h"
#import "SidebarViewController.h"
#import "UIImageView+WebCache.h"


@interface MainViewController ()<SWRevealViewControllerDelegate>
{
    BOOL editingmode;
    UIButton *editbtn;
    UIImageView *buttonimageview;
    int currentField;
    CGRect screenBounds;
    CGPoint pointtrack,pointtrackcontentoffset;
    CGSize kbsize,contentsize;
    UIView *paddingView;
    UIBarButtonItem *oldButton;
    BOOL changedpwdappearscreen;
    UIView *changepwdview;
    UITextField *txt1,*txt2,*txt3;
    
}

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma set delegate for reveal
    self.rowSelectionDelegate = (SidebarViewController *)self.revealViewController.rearViewController;
#pragma store bydefault value
    pointtrackcontentoffset=self.aScrollview.contentOffset;
    screenBounds = [[UIScreen mainScreen] bounds];
    contentsize=self.aScrollview.contentSize;
    changedpwdappearscreen=FALSE;
    
#pragma maintain keyboard notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
#pragma  by default false boolean
    
    editingmode=FALSE;
    
#pragma apply padding
    for (id txt in self.aScrollview.subviews)
    {
        if([txt isKindOfClass:[UITextField class]])
        {
            UITextField *local=(UITextField *)txt;
            paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            local.leftView = paddingView;
            local.leftViewMode = UITextFieldViewModeAlways;
        }
    }
#pragma made round image view
    _profileImgview.layer.cornerRadius=_profileImgview.frame.size.width/2;
    _profileImgview.layer.borderColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0].CGColor;
    _profileImgview.layer.borderWidth=2.0;
    
    // set textview allignment
  //  self.txtviewAddress.frame=CGRectMake(self.txtviewAddress.frame.origin.x, self.addresslabel.frame.origin.y-2, self.txtviewAddress.frame.size.width, self.txtviewAddress.frame.size.height);
    
    
    [self.rowSelectionDelegate selectRowWithHeading:0];
    [self.rowSelectionDelegate selectRowWithHeading:1];
    self.title = @"PROFILE";
    [self setCustomNavigationButton];
    
     [self loaddata];
}
#pragma cutomize navigation
-(void)setCustomNavigationButton
{
    
    [self.navigationController.navigationBar setCustomNavigationButton:self];
#pragma adding editiable button
    
    editbtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 46, 44)];
    [editbtn addTarget:self action:@selector(editable) forControlEvents:UIControlEventTouchUpInside];
    //editbtn.backgroundColor=[UIColor redColor];
    buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(13,12, 20, 20)];
   // buttonimageview.backgroundColor=[UIColor yellowColor];
    buttonimageview.image=[UIImage imageNamed:@"edit_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFill;
    [editbtn addSubview:buttonimageview];
    
    UIBarButtonItem *editablebarbtn = [[UIBarButtonItem alloc] initWithCustomView:editbtn];
    editablebarbtn.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItems = @[editablebarbtn];
    
    // new code
    oldButton = self.navigationItem.rightBarButtonItem;
    
    self.revealViewController.delegate=self;
    
}

#pragma swrevealview delegate
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    [self dismisskeyboard];
}

// editable method
-(void)editable
{
  [self.activeTextfield resignFirstResponder];
    
  if(!editingmode)
  {
      editingmode=TRUE;
      buttonimageview.image=[UIImage imageNamed:@"save_icon.png"];
      _txtEmail.userInteractionEnabled=YES;
      _txtEmail.layer.borderWidth=1.0;
      _txtEmail.layer.borderColor=[UIColor blackColor].CGColor;
      _txtName.userInteractionEnabled=YES;
      _txtName.layer.borderWidth=1.0;
      _txtName.layer.borderColor=[UIColor blackColor].CGColor;
      _txtPhone.userInteractionEnabled=NO;
      _txtviewAddress.userInteractionEnabled=YES;
      _txtviewAddress.layer.borderWidth=1.0;
      _txtviewAddress.layer.borderColor=[UIColor blackColor].CGColor;
      
#pragma camera btn manage
      _photoeditimage.hidden=FALSE;
      _btnSelectpic.userInteractionEnabled=YES;
      
  }

 else
 {
      editingmode=FALSE;
      buttonimageview.image=[UIImage imageNamed:@"edit_icon.png"];
     _txtEmail.userInteractionEnabled=NO;
     _txtEmail.layer.borderWidth=0.0;
     _txtEmail.layer.borderColor=[UIColor clearColor].CGColor;
     _txtName.userInteractionEnabled=NO;
     _txtName.layer.borderWidth=0.0;
     _txtName.layer.borderColor=[UIColor clearColor].CGColor;
     _txtPhone.userInteractionEnabled=NO;
     _txtviewAddress.userInteractionEnabled=NO;
     _txtviewAddress.layer.borderWidth=0.0;
     _txtviewAddress.layer.borderColor=[UIColor clearColor].CGColor;
#pragma camera btn manage
     _photoeditimage.hidden=TRUE;
     _btnSelectpic.userInteractionEnabled=NO;
     
     // new code
     
     if (!changedpwdappearscreen)
     {
         UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Do you want to save your details?" message:nil delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
         alert.tag=101;
         [alert show];
         
         // [self updateprofile];
     }
    
     
 }
}
#pragma update passwod
-(void)updateprofile
{
    
//update: Api:: "http://dev1.businessprodemo.com/hbcr/php/webservice.php?operation=updateCustomerDetails";
//    METHOD_TYPE_POST;
//    keys = {"userid","name","emailId","address","imagename"};
    

    NSString *path =[NSString stringWithFormat:@"%@updateCustomerDetails",app->parentUrl];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait a moment..."];
    if(!selectedimage)
    {
        if([_profileImgview.image isEqual:[UIImage imageNamed:@"profile_img1.png"]])
        {
            selectedimage=nil;
        }
        else
        {
            selectedimage=_profileImgview.image;
        }
    }
    
    
    imageDataPicker=(NSData *)UIImagePNGRepresentation(selectedimage);
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"Account"] valueForKey:@"accountid"],@"userid",
                                _txtName.text,@"name",
                                _txtEmail.text,@"emailId",
                                _txtviewAddress.text,@"address",
                                app->customersessionname,@"sessionName",
                                nil];
    
    AFHTTPRequestOperation *op = [manager POST:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                  {
                                      
                                     
                                      if(imageDataPicker)
                                      {
                                          [formData appendPartWithFileData:imageDataPicker name:@"imagename" fileName:@"photo.png" mimeType:@"image/png"];
                                      }
                                  }
                                       success:^(AFHTTPRequestOperation *operation, id responseObject)
                                  {
                                      NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
                                      NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
                                      NSLog(@"%@",responseStr);
                                      NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
                                      NSLog(@"%@",dict);
                                      //here is place for code executed in success case
                                      if([[dict valueForKey:@"success"]integerValue]==0)
                                      {
                                          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                          [alert show];
                                          [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                          [SVProgressHUD dismiss];
                                      }
                                      else
                                      {
                                          [self updateuserdetailservice];
                                          
                                      }
                                      
                                  }
                                       failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                  
                                  {
                                      [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                      NSLog(@"Error: %@ ***** %@", operation.responseString, error);
                                      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                                      [alert show];
                                      [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                                      [SVProgressHUD dismiss];
                                  }
                                  ];
    [op start];
    
    
}
-(void)updateuserdetailservice
{
    
    
    NSString *path =[NSString stringWithFormat:@"%@getUserInfo",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
  
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         // NSLog(@"%@",dict);
#pragma store customer value in global dictionary
         app->globalcustomerdetail=[dict valueForKey:@"result"];
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
             
         }
         else
         {
#pragma store username in user default
             if(app->userdefaultcheck)
             {
                 NSUserDefaults *default1=[NSUserDefaults standardUserDefaults];
                 [default1 setValue:app->customerusername forKey:@"customerusername"];
                 [default1 setValue:app->customertoken forKey:@"customertoken"];
                 [default1 setValue:app->customersessionname forKey:@"customersession"];
                 [default1 setValue:app->customeraccesskey forKey:@"customeraccesskey"];
              //   [default1 setValue:app->MD5string forKey:@"MD5key"];
                 [default1 synchronize];
             }
             
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Profile Updated Sucessfully" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error){
             
             //here is place for code executed in success case
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             NSLog(@"Error: %@", [error localizedDescription]);
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }];
    
}



#pragma added observer selector
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    kbsize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.aScrollview setContentSize:CGSizeMake(self.view.bounds.size.width,self.aScrollview.frame.size.height+kbsize.height)];
    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [self.aScrollview setContentOffset:pointtrackcontentoffset animated:NO];
    [self.aScrollview setContentSize:contentsize];
}

#pragma textfield and textview delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentField=0;
    self.activeTextfield=textField;
    self.activeTextview=nil;
    
    if (textField==_txtEmail) {
        
        pointtrack=_view_EmailId.frame.origin;
    }
    
    
    else{
    
    pointtrack = textField.frame.origin;
        
    }
    
#pragma add toolbar
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 1000;
    toolBar.barTintColor=[UIColor colorWithRed:30/255.0f green:87/255.0f blue:140/255.0f alpha:1.0f];
    toolBar.tintColor=[UIColor whiteColor];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismisskeyboard)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];

    
#pragma maintain chila and parent view
if(!changedpwdappearscreen)
{
#pragma add accessory
    [_txtName setInputAccessoryView:toolBar];
    [_txtEmail setInputAccessoryView:toolBar];
    [_txtPhone setInputAccessoryView:toolBar];
    if(screenBounds.size.height==480)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-30) animated:YES];
    }
    else if(screenBounds.size.height==568)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-80) animated:YES];
    }
    else if(screenBounds.size.height==667)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-136) animated:YES];
    }
    else if(screenBounds.size.height==736)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-173) animated:YES];
    }
    else
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-300) animated:YES];
    }
}
    else
    {
#pragma add accessory
        for (id txt in changepwdview.subviews)
        {
            if([txt isKindOfClass:[UITextField class]])
            {
                UITextField *settextfield=(UITextField *)txt;
                [settextfield setInputAccessoryView:toolBar];
            
            }
        }
        
        CGRect rect=changepwdview.frame;
        rect.origin=pointtrackcontentoffset;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        if(screenBounds.size.height==480)
        {
            rect.origin.y-=pointtrack.y-25;
        }
        else if(screenBounds.size.height==568)
        {
            rect.origin.y-=pointtrack.y-50;
        }
        else if(screenBounds.size.height==667)
        {
            if(pointtrack.y>150.0)
            {
            rect.origin.y-=pointtrack.y-100;
            }
        }
        else if(screenBounds.size.height==736)
        {
            if(pointtrack.y>150.0)
            {
                rect.origin.y-=pointtrack.y-120;
            };
        }
        else
        {
            
        }
        changepwdview.frame=rect;
        [UIView commitAnimations];
        
        
    }
}
-(void)dismisskeyboard
{
    if(!changedpwdappearscreen)
    {
    if(currentField==0)
    {
    [[self.view viewWithTag:1000] removeFromSuperview];
    [self.activeTextfield resignFirstResponder];
    }
    else
    {
    [[self.view viewWithTag:1001] removeFromSuperview];
    [self.activeTextview resignFirstResponder];
    }
    }
    else
    {
    CGRect rect=changepwdview.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    changepwdview.frame=rect;
    [UIView commitAnimations];
    [self.activeTextfield resignFirstResponder];
    }
   
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(currentField==0)
    {
    
        long tag=self.activeTextfield.tag;
        tag++;
        UITextField *txt=(UITextField *)[self.view viewWithTag:tag];
        if(txt==nil)
        {
            [self dismisskeyboard];
        }
        else
        {
           [txt becomeFirstResponder];
        }
    }
    else if(currentField==1)
    {
        
        long tag=self.activeTextview.tag;
        tag++;
        UITextView *txt=(UITextView *)[self.view viewWithTag:tag];
        if(txt==nil)
        {
            [self dismisskeyboard];
        }
        else
        {
            [txt becomeFirstResponder];
        }
    }
   
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    currentField=1;
    self.activeTextview=textView;
    self.activeTextfield=nil;
    
    if (textView==_txtviewAddress) {
        pointtrack=_view_Address.frame.origin;
    }else{
    
    
    pointtrack = textView.frame.origin;
        
    }
#pragma add toolbar
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 1001;
    toolBar.barTintColor=[UIColor colorWithRed:30/255.0f green:87/255.0f blue:140/255.0f alpha:1.0f];
    toolBar.tintColor=[UIColor whiteColor];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismisskeyboard)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
#pragma add accessory
    [_txtviewAddress setInputAccessoryView:toolBar];
    [_txtviewAddress reloadInputViews];
    
    if(screenBounds.size.height==480)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-30) animated:YES];
    }
    else if(screenBounds.size.height==568)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-80) animated:YES];
    }
    else if(screenBounds.size.height==667)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-136) animated:YES];
    }
    else if(screenBounds.size.height==736)
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-173) animated:YES];
    }
    else
    {
        [self.aScrollview setContentOffset:CGPointMake(0, pointtrack.y-300) animated:YES];
    }
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
//    
//#pragma Load user data
   
    
}
-(void)loaddata
{
    // new code
    editbtn.enabled=NO;
    
    
    NSLog(@"%@",[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"]);
    NSString *append=[NSString stringWithFormat:@" %@",[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"last_name"]];
    _txtName.text=[[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"first_name"] stringByAppendingString:append];
    _txtPhone.text=[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"phone_mobile"];
    _txtEmail.text=[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"email1"];
#pragma manage address as shown
   // NSString *mainaddress=[NSString stringWithFormat:@"%@, %@ -%@, %@",[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"address_street"],[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"address_city"],[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"address_postalcode"],[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"address_state"]];
    
    NSString *mainaddress=[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"address_street"];
    
    
    _txtviewAddress.text=mainaddress;
    
    
  //   [_profileImgview sd_setImageWithURL:[NSURL URLWithString:[app->globalcustomerdetail valueForKeyPath:@"data.User.user_image_url"]] placeholderImage:[UIImage imageNamed:@"profile_img1.png"]];
    
    
    
    
    NSString *imagepath=[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"user_image_url"];
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.color=[UIColor redColor];
    activityIndicator.center = CGPointMake(CGRectGetMidX(_profileImgview.bounds), CGRectGetMidY(_profileImgview.bounds));
    activityIndicator.hidesWhenStopped = YES;
    
     __weak UIButton *editweakbtn = editbtn;
    __block BOOL successfetchimage=FALSE;
    __weak UIImageView *setimage = _profileImgview;
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
    [_profileImgview setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"profile_img1.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         setimage.contentMode=UIViewContentModeScaleAspectFill;
         setimage.image=image;
         editweakbtn.enabled=YES;
         successfetchimage=TRUE;
         [activityIndicator removeFromSuperview];
     }
                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
     {
         editweakbtn.enabled=YES;
         [activityIndicator removeFromSuperview];
     }];
    if(!successfetchimage)
    {
        if(![imagepath isEqualToString:@""]||imagepath)
        {
            editweakbtn.enabled=NO;
            [_profileImgview addSubview:activityIndicator];
            [activityIndicator startAnimating];
        }
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapimageedit:(id)sender
{
    [self dismisskeyboard];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Upload Image \nFrom:" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Camera",@"Gallery", nil];
    alert.tag=100;
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==100)
    {
        if(buttonIndex==1)
        {
            [self takepicture];
        }
        else if(buttonIndex==2)
        {
            [self selectgallery];
        }
    }
    else if(alertView.tag==101)
    {
        if(buttonIndex==0)
        {
            [self updateprofile];
        }
        else
        {
            [self loaddata];
        }
    }
    
}
-(void)selectgallery
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    ImagePicker=[[UIImagePickerController alloc]init];
    ImagePicker.delegate=(id)self;
    ImagePicker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:ImagePicker animated:YES completion:nil];
}
-(void)takepicture
{
    /******************** code for camera ***********************/
    
    ImagePicker=[[UIImagePickerController alloc]init];
    if    (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
    else
    {
        
        ImagePicker.delegate=(id)self;
        ImagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
        // imagepic.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:ImagePicker animated:YES completion:nil];
        NSLog(@"camera");
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // new code
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [picker dismissViewControllerAnimated:YES completion:nil];
    // [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    selectedimage=[info objectForKey:UIImagePickerControllerOriginalImage];
    selectedimage=[UIImage scaleAndRotateImage:selectedimage];
    _profileImgview.contentMode=UIViewContentModeScaleAspectFill;
    _profileImgview.image=selectedimage;
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didTapChangepwd:(id)sender
{
    NSLog(@"clicked");
    _btnchangepwd.hidden=TRUE;
    self.navigationItem.rightBarButtonItem=nil;
    changedpwdappearscreen=TRUE;
    [self hideinstance];
    [self createcustomviewpwd];
}
#pragma create customview
-(void)createcustomviewpwd
{
    if(editingmode)
    {
        [self editable];
    }
    
    if([self.view viewWithTag:10000])
    {
        return;
    //[[self.view viewWithTag:10000] removeFromSuperview];
    }
    changepwdview=[[UIView alloc] initWithFrame:CGRectMake(40,100,self.view.frame.size.width-80,260)];
    changepwdview.tag=10000;
    CGRect bounds = self.view.bounds;
    changepwdview.center = CGPointMake(CGRectGetMidX(bounds), CGRectGetMidY(bounds));
    changepwdview.backgroundColor=[UIColor whiteColor];
    
    UILabel *heading=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, changepwdview.frame.size.width, 40)];
    heading.text=@"Change Password";
    heading.textAlignment=NSTextAlignmentCenter;
    [heading setFont:[UIFont systemFontOfSize:18.0]];
    heading.textColor=[UIColor whiteColor];
    heading.backgroundColor=[UIColor blackColor];
    [changepwdview addSubview:heading];
    
    
    txt1=[[UITextField alloc] initWithFrame:CGRectMake(5, 60, changepwdview.frame.size.width-10, 30)];
    txt1.tag=5;
    txt1.delegate=self;
    txt1.borderStyle=UITextBorderStyleLine;
    txt1.placeholder=@"Enter Old Password";
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txt1.leftView = paddingView;
    txt1.leftViewMode = UITextFieldViewModeAlways;
    [txt1 setFont:[UIFont boldSystemFontOfSize:14.0]];
    txt1.returnKeyType=UIReturnKeyNext;
    txt1.secureTextEntry=YES;
    
    txt2=[[UITextField alloc] initWithFrame:CGRectMake(5, 110, changepwdview.frame.size.width-10, 30)];
    txt2.tag=6;
    txt2.delegate=self;
    txt2.borderStyle=UITextBorderStyleLine;
    txt2.placeholder=@"Enter New Password";
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txt2.leftView = paddingView;
    txt2.leftViewMode = UITextFieldViewModeAlways;
    [txt2 setFont:[UIFont boldSystemFontOfSize:14.0]];
    txt2.returnKeyType=UIReturnKeyNext;
    txt2.secureTextEntry=YES;
    
    txt3=[[UITextField alloc] initWithFrame:CGRectMake(5, 160, changepwdview.frame.size.width-10, 30)];
    txt3.tag=7;
    txt3.delegate=self;
    txt3.borderStyle=UITextBorderStyleLine;
    txt3.placeholder=@"Re-enter Password";
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    txt3.leftView = paddingView;
    txt3.leftViewMode = UITextFieldViewModeAlways;
    [txt3 setFont:[UIFont boldSystemFontOfSize:14.0]];
    txt3.returnKeyType=UIReturnKeyDone;
    txt3.secureTextEntry=YES;
    
    [changepwdview addSubview:txt1];
    [changepwdview addSubview:txt2];
    [changepwdview addSubview:txt3];
    
    UIButton *save=[UIButton buttonWithType:UIButtonTypeCustom];
    save.frame=CGRectMake(10, 220, (changepwdview.frame.size.width/2)-15, 30);
   // save.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"button.png"]];
    save.backgroundColor=[UIColor colorWithRed:10/255.0f green:49/255.0f blue:103/255.0f alpha:1.0f];
    [save setTitle:@"Save" forState:UIControlStateNormal];
    [save addTarget:self action:@selector(updatepassword) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIButton *cancel=[UIButton buttonWithType:UIButtonTypeCustom];
    cancel.frame=CGRectMake((changepwdview.frame.size.width/2)+5, 220, (changepwdview.frame.size.width/2)-15, 30);
    cancel.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"button.png"]];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(dismissview) forControlEvents:UIControlEventTouchUpInside];
    
   
    
    [changepwdview addSubview:cancel];
    [changepwdview addSubview:save];
    //[self.view addSubview:changepwdview];
    
    [UIView transitionWithView:self.view
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:^
    {
                        changepwdview.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
                        [self.view addSubview:changepwdview];
                    } completion:nil];
    
    // new code
    pointtrackcontentoffset=changepwdview.frame.origin;
    
}
-(void)updatepassword
{
    if(![NSString validation:txt1.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill old password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if(![NSString validation:txt2.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill new password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if(![NSString validation:txt3.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill re-enter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if(![txt1.text isEqualToString:app->customerpwd])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill correct old password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        txt1.text=@"";
        return;
    }
    else if(![txt2.text isEqualToString:txt3.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill correct re-enter password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        txt3.text=@"";
        [alert show];
        return;
    }
    
    app->customerpwd=txt2.text;
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    [def setValue:app->customerpwd forKey:@"customerpassword"];
    [def synchronize];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Password Changed Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    txt1.text=@"";
    txt2.text=@"";
    txt3.text=@"";
    [self dismissview];
    
}

-(void)dismissview
{
    _btnchangepwd.hidden=FALSE;
    self.navigationItem.rightBarButtonItem=oldButton;
    changedpwdappearscreen=FALSE;
    pointtrackcontentoffset=self.aScrollview.contentOffset;
    [self showinstance];
    [[self.view viewWithTag:10000] removeFromSuperview];
}
-(void)hideinstance
{
    // new code
    for (id v in self.aScrollview.subviews)
    {
        if([v isKindOfClass:[UILabel class]])
        {
        UILabel *txt=(UILabel *)v;
        txt.hidden=TRUE;
        }
        else if([v isKindOfClass:[UITextField class]])
        {
            UITextField *txt=(UITextField *)v;
            txt.hidden=TRUE;
        }
        else if([v isKindOfClass:[UITextView class]])
        {
            UITextView *txt=(UITextView *)v;
            txt.hidden=TRUE;
        }
    }
}
-(void)showinstance
{
    // new code
    for (id v in self.aScrollview.subviews)
    {
        if([v isKindOfClass:[UILabel class]])
        {
            UILabel *txt=(UILabel *)v;
            txt.hidden=FALSE;
        }
        else if([v isKindOfClass:[UITextField class]])
        {
            UITextField *txt=(UITextField *)v;
            txt.hidden=FALSE;
        }
        else if([v isKindOfClass:[UITextView class]])
        {
            UITextView *txt=(UITextView *)v;
            txt.hidden=FALSE;
        }
    }
}
#pragma remove observer
-(void)dealloc
{
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}


//-(void)updateuserdetailservice
//{
//    
//    
//    NSString *path =[NSString stringWithFormat:@"%@getUserInfo",app->parentUrl];
//    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",nil];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//  
//    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         
//         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
//         NSLog(@"%@",responseStr);
//         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
//         // NSLog(@"%@",dict);
//#pragma store customer value in global dictionary
//         app->globalcustomerdetail=[dict valueForKey:@"result"];
//         //here is place for code executed in success case
//         if([[dict valueForKey:@"success"]integerValue]==0)
//         {
//             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//             [SVProgressHUD dismiss];
//             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//             [alert show];
//             
//             
//         }
//         else
//         {
//#pragma store username in user default
//             if(app->userdefaultcheck)
//             {
//                 NSUserDefaults *default1=[NSUserDefaults standardUserDefaults];
//                 [default1 setValue:app->customerusername forKey:@"customerusername"];
//                 [default1 setValue:app->customertoken forKey:@"customertoken"];
//                 [default1 setValue:app->customersessionname forKey:@"customersession"];
//                 [default1 setValue:app->customeraccesskey forKey:@"customeraccesskey"];
//                 //[default1 setValue:app->MD5string forKey:@"MD5key"];
//                 [default1 synchronize];
//             }
//             
//         }
//         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//         [SVProgressHUD dismiss];
//         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Profile Updated Sucessfully" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//         [alert show];
//         
//     }
//         failure:^(AFHTTPRequestOperation *operation, NSError *error){
//             
//             //here is place for code executed in success case
//             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//             [SVProgressHUD dismiss];
//             NSLog(@"Error: %@", [error localizedDescription]);
//             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//             [alert show];
//         }];
//    
//}
#pragma mark -- ForLandscape and portrait issue
- (NSUInteger) supportedInterfaceOrientations
{
    //    //Because your app is only landscape, your view controller for the view in your
    //    // popover needs to support only landscape
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate {
    return NO;
}


@end
