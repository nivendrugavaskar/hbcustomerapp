//
//  MyprogressTableViewCell.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 22/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface MyprogressTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *divider;
@property (strong, nonatomic) IBOutlet UILabel *yoganame;
@property (strong, nonatomic) IBOutlet UILabel *bestratinglabel;
@property (strong, nonatomic) IBOutlet UILabel *avgratinglabel;

@end
