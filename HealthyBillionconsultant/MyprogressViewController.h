//
//  MyprogressViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 22/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MyprogressTableViewCell.h"
#import "RatingView.h"

@interface MyprogressViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,RatingViewDelegate,UIScrollViewDelegate>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITableView *aTableview;
@property (strong, nonatomic) IBOutlet UILabel *feedbackheading;

@end
