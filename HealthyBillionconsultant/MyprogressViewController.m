//
//  MyprogressViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 22/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "MyprogressViewController.h"
#import "RatingView.h"
#import "StarRatingView.h"
#import "GraphviewViewController.h"


#define kLabelAllowance 50.0f
#define kStarViewHeight 10.0f
#define kStarViewWidth 50.0f
#define kLeftPadding 5.0f


@interface MyprogressViewController ()
{
    NSMutableArray *customerarray,*sessionarray,*feedbacktextarray;
    float cellheight;
    NSDictionary *dict;
    NSInteger pagenumber;
    
    bool notcreatestar,enddata;;
}

@property (strong, nonatomic) RatingView *ratingView;

@end

@implementation MyprogressViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    [self.navigationController.navigationBar setCustomNavigationButton:self];
    self.title=@"MY PROGRESS";
    
    
   //customerarray=[[NSMutableArray alloc] initWithObjects:@"Customer1",@"Customer2",@"Customer3",@"Customer4", nil];
   customerarray=[[NSMutableArray alloc] init];
//   sessionarray=[[NSMutableArray alloc] initWithObjects:@"Session1",@"Session2",@"Session3",@"Session4", nil];
//   feedbacktextarray=[[NSMutableArray alloc] initWithObjects:@"1st Line",@" How are you bjjkfkdksgfkdgskfgdks  kshdjkfhjks 2nd line",@"kjdsfjkhsdjkfhjkdshfjkbdhskfbjksdhfjkdshfjkhdsjkfhjkdshfjkdhskj fhjkdshfjkdhsjkfhdjkshfjkskflsdhflkhdlshfldshfldshflhdsklfhsdlfdslfsldfhldshflhsdlfsdgfldsgfldgslfgdlsgflsdgflgdslfglsdgflgsdlfgdlsf ending here",@"kjdsfjkhsdjkfhjkdshfjkbdhskfbjksdhfjkdshfjkhdsjkfhjkdshfjkdhskj fhjkdshfjkdhsjkfhdjkshfjkskflsdhflkhdlshfldshfldshflhdsklfhsdlfdslfsldfhldshflhsdlfsdgfldsgfldgslfgdlsgflsdgflgdslfglsdgflgsdlfgdlsf ending here", nil];
    self.feedbackheading.layer.cornerRadius=5.0;
    self.feedbackheading.layer.borderWidth=1.0;
    self.feedbackheading.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    self.feedbackheading.clipsToBounds=YES;
    
#pragma test line graph
//    // add back button
//    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [custombutton addTarget:self action:@selector(sendtootherpage) forControlEvents:UIControlEventTouchUpInside];
//    custombutton.backgroundColor=[UIColor clearColor];
//    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
//    buttonimageview.image=[UIImage imageNamed:@"My_Progress_icon.png"];
//    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
//    [custombutton addSubview:buttonimageview];
//    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
//    backbarbutton.style = UIBarButtonItemStylePlain;
//    self.navigationItem.rightBarButtonItems = @[backbarbutton];
    
#pragma feedback service
    [self yogaservice];
    
}
-(void)restructtable
{
    self.aTableview.layer.cornerRadius=5.0;
    self.aTableview.layer.borderWidth=1.0;
    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
}
#pragma feedback service
-(void)yogaservice
{

    
    NSString *path =[NSString stringWithFormat:@"%@getCustomersListOfTask",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"Account"] valueForKey:@"accountid"],@"accountid",[NSString stringWithFormat:@"%d",(int)(pagenumber*10)],@"limit_from",@"10",@"limit_to",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Loading.."];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         dict=[[NSDictionary alloc] init];
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
             
         }
         else
         {
             if([dict valueForKey:@"result"]==nil||[[dict valueForKey:@"result"]isKindOfClass:[NSNull class]])
             {
                 //                 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 //                 [SVProgressHUD dismiss];
                 pagenumber--;
                 enddata=TRUE;
             }
             else
             {
                 enddata=FALSE;
                 NSMutableArray *localarray=[[dict valueForKey:@"result"] mutableCopy];
                 if([localarray count]>0)
                 {
                     for(int i=0;i<[localarray count];i++)
                     {
                         [customerarray addObject:[localarray objectAtIndex:i]];
                     }
                 }
                 [self restructtable];
                 [self.aTableview reloadData];
             }
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}

//-(void)sendtootherpage
//{
//    
//    [self performSegueWithIdentifier:@"pushtograph" sender:self];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    MyprogressTableViewCell *cell=(MyprogressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    //cell.contentView.backgroundColor=[UIColor redColor];
    
    if(cell==nil)
    {
        cell=[[MyprogressTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    
    @try
    {
        cell.yoganame.text=[[[customerarray objectAtIndex:indexPath.row] valueForKey:@"tasksname"] mutableCopy];
        if([[[customerarray objectAtIndex:indexPath.row] valueForKey:@"avgratings"] isKindOfClass:[NSNull class]])
        {
             cell.avgratinglabel.text=@"Average Rating: No Rating";
            
        }
        else
        {
           cell.avgratinglabel.text=[NSString stringWithFormat:@"Average Rating: %@",[[customerarray objectAtIndex:indexPath.row] valueForKey:@"avgratings"]];
        }
        if([[[customerarray objectAtIndex:indexPath.row] valueForKey:@"maxratings"]isKindOfClass:[NSNull class]])
        {
            
            cell.bestratinglabel.text=@"Best Rating: No Rating";
        }
        else
        {
            cell.bestratinglabel.text=[NSString stringWithFormat:@"Best Rating: %@",[[customerarray objectAtIndex:indexPath.row] valueForKey:@"maxratings"]];
        }
        
        cell.divider.frame=CGRectMake(cell.divider.frame.origin.x, cell.divider.frame.origin.y, self.aTableview.frame.size.width, cell.divider.frame.size.height);
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    cellheight=95.0;
    return cellheight;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if(scrollView.contentOffset.y+scrollView.frame.size.height==scrollView.contentSize.height)
    {
        if(!enddata)
        {
            NSLog(@"call service here");
            pagenumber++;
            [self yogaservice];
        }
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [customerarray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Deselect cell
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    
     if([[[customerarray objectAtIndex:indexPath.row] valueForKey:@"maxratings"]isKindOfClass:[NSNull class]]||[[[customerarray objectAtIndex:indexPath.row] valueForKey:@"avgratings"] isKindOfClass:[NSNull class]])
     {
         UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"No Data" message:@"You have no performance data" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
         return;
     }
    
    [self performSegueWithIdentifier:@"pushtograph" sender:indexPath];
   
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"pushtograph"])
    {
        if([sender isKindOfClass:[NSIndexPath class]])
        {
            NSIndexPath *index=(NSIndexPath *)sender;
            GraphviewViewController *instace=(GraphviewViewController *)segue.destinationViewController;
            instace.graphdataarray=[customerarray objectAtIndex:index.row];
           // instace.customerid=scheduleuserid;
           // instace.sessionid=scheduleusersessionid;
        }
    }
 }


- (void)viewDidUnload
{
   
    [super viewDidUnload];
}



@end
