//
//  ScheduledetailsViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 23/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "scheduledetailsTableViewCell.h"
#import "yogalistViewController.h"

@interface ScheduledetailsViewController : UIViewController<UITextFieldDelegate,yogalistprotocol>
{
    AppDelegate *app;
    BOOL hudmaintain;
}

- (IBAction)btnAction_BackDateArrow:(id)sender;
- (IBAction)btnAction_FutureDateArrow:(id)sender;

- (IBAction)btnAction_ActivateDatePickerKeyboard:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btndatepickershow;


//@property (weak, nonatomic) IBOutlet UITextField *txtFld_DatePicker;

@property (strong,nonatomic)NSDate *selecteddate;
@property (strong, nonatomic) IBOutlet UILabel *datelabel;
@property (strong, nonatomic) IBOutlet UITableView *aTableview;

@property(strong,nonatomic)NSMutableArray *tableviewArray;
@property (strong, nonatomic) IBOutlet UILabel *noschedulelabel;
@property (strong, nonatomic) IBOutlet UIButton *btnbackdate;
@property (strong, nonatomic) IBOutlet UIButton *btnnextdate;

@end
