//
//  ScheduledetailsViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 23/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "ScheduledetailsViewController.h"

@interface ScheduledetailsViewController ()
{
    NSMutableArray *schedulearray;
    NSDictionary *dict;
    
    UIDatePicker *datePicker;
    
    NSMutableArray *serviceSchedulearray;
    
    int valueIncreaseDate;
    int valueDecreaseDate;
    
    int dateIncreaseDecrease;
    
    
}

@end

@implementation ScheduledetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    valueDecreaseDate=0;
    valueIncreaseDate=0;
    dateIncreaseDecrease=0;
    _noschedulelabel.hidden=YES;
    _btndatepickershow.layer.cornerRadius=5.0;
    self.title=@"SCHEDULE DETAILS";
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    schedulearray=[[NSMutableArray alloc] init];
    
  //  NSLog(@"Table View array %@",_tableviewArray.description);
    
    self.aTableview.layer.cornerRadius=5.0;
    self.aTableview.layer.borderWidth=1.0;
    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    
    self.noschedulelabel.layer.cornerRadius=5.0;
    self.noschedulelabel.layer.borderWidth=1.0;
    self.noschedulelabel.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    
    
#pragma checking date
    if(_selecteddate)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd MMMM yyyy"];
        
        NSString *strDate = [formatter stringFromDate:_selecteddate];
        NSLog(@"selected date=%@",strDate);
        self.datelabel.text=strDate;
    }
    
    self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    [self.navigationItem setHidesBackButton:YES];
    // add back button
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    backbarbutton.style = UIBarButtonItemStylePlain;
    self.navigationItem.leftBarButtonItems = @[backbarbutton];
    hudmaintain=FALSE;
    // new code
    _btndatepickershow.backgroundColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0];
    [self scheduleDateWiseData];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self scheduleDateWiseData];
}

-(void)scheduleDateWiseData
{
    if(!hudmaintain)
    {
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
    //yyyy-MM-dd
    NSLog(@"Date Label %@",_datelabel.text);
    NSString *path =[NSString stringWithFormat:@"%@dateWiseCustomerSessions",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",_datelabel.text,@"date",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         dict=[[NSDictionary alloc] init];
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
             
         }
         else
         {
             
             
             if ([dict objectForKey:@"result"]!=[NSNull null])
             {
                 serviceSchedulearray=[dict objectForKey:@"result"];
                 
                 if([serviceSchedulearray count]>0)
                 {
                     _aTableview.hidden=FALSE;
                     _noschedulelabel.hidden=TRUE;
                     // schedulearray=_tableviewArray;
                     
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [self.aTableview reloadData];
                         
                         
                     });
                 }
                 else
                 {
                     _aTableview.hidden=TRUE;
                     _noschedulelabel.hidden=FALSE;
                 }
                
             }
             else
             {
                _noschedulelabel.hidden=FALSE;
                 _aTableview.hidden=TRUE;
             }
        
         }
         

         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pushtoyogalist"])
    {
        if([sender isKindOfClass:[NSIndexPath class]])
        {
            NSIndexPath *index=(NSIndexPath *)sender;
            yogalistViewController *instace=(yogalistViewController *)segue.destinationViewController;
           // instace.selecteddate=_datelabel.text;
            instace.delegateyoga=self;
            instace.yogalistarray=[serviceSchedulearray objectAtIndex:index.row];
            instace.customerid=[[serviceSchedulearray valueForKey:@"accountid"]objectAtIndex:index.row];
            instace.sessionid=[[serviceSchedulearray valueForKey:@"sessionsid"]objectAtIndex:index.row];
            
            
            
        }
    }
    
}

// delegate method yoga list
-(void)buttonTappedfeedback
{
    hudmaintain=TRUE;
    [self scheduleDateWiseData];
}

#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    scheduledetailsTableViewCell *cell=(scheduledetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    if(cell==nil)
    {
        cell=[[scheduledetailsTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    
    @try
    {
        cell.schedullabel.frame=CGRectMake(5, cell.schedullabel.frame.origin.y, self.aTableview.frame.size.width-10, cell.schedullabel.frame.size.height);
        cell.lbl_SessionStatus.frame=CGRectMake(5, cell.lbl_SessionStatus.frame.origin.y, self.aTableview.frame.size.width-97, cell.lbl_SessionStatus.frame.size.height);
        cell.lbl_Address.frame=CGRectMake(5, cell.lbl_Address.frame.origin.y, self.aTableview.frame.size.width-10, cell.lbl_Address.frame.size.height);
        cell.divider.frame=CGRectMake(0, cell.divider.frame.origin.y, self.aTableview.frame.size.width, cell.divider.frame.size.height);
        cell.datelabel.frame=CGRectMake(self.aTableview.frame.size.width-96, cell.datelabel.frame.origin.y, cell.datelabel.frame.size.width, cell.datelabel.frame.size.height);
        cell.datelabe11.frame=CGRectMake(self.aTableview.frame.size.width-96, cell.datelabe11.frame.origin.y, cell.datelabe11.frame.size.width, cell.datelabe11.frame.size.height);
        cell.textarrow.frame=CGRectMake(self.aTableview.frame.size.width-24, cell.textarrow.frame.origin.y, cell.textarrow.frame.size.width, cell.textarrow.frame.size.height);
        cell.schedullabel.text=[[serviceSchedulearray objectAtIndex:indexPath.row] valueForKey:@"accountname"];
        cell.lbl_Address.text=[[serviceSchedulearray objectAtIndex:indexPath.row]valueForKey:@"address"];
        cell.lbl_SessionStatus.text=[NSString stringWithFormat:@"Status: %@",[[serviceSchedulearray objectAtIndex:indexPath.row]valueForKey:@"sessionstatus"]];
        cell.datelabel.text=[[serviceSchedulearray objectAtIndex:indexPath.row] valueForKey:@"start_time"];
        cell.datelabe11.text=[[serviceSchedulearray objectAtIndex:indexPath.row] valueForKey:@"end_time"];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 110.0;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [serviceSchedulearray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[[serviceSchedulearray objectAtIndex:indexPath.row] valueForKey:@"sessionstatus"]isEqualToString:@"Held"]) {
        
        
        
        [self performSegueWithIdentifier:@"pushtoyogalist" sender:indexPath];
        
        
    }else{
        
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Not Attended" message:@"Session is not Held yet" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    
    
    
    
    
}

- (IBAction)btnAction_BackDateArrow:(id)sender
{

    if (dateIncreaseDecrease >= -2) {
        
        NSLog(@"Print present Date %@",_datelabel.text);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd MMMM yyyy"];
        NSDate *date = [formatter dateFromString:_datelabel.text];
        
        NSDate *decreaseDate= [NSDate dateWithTimeInterval:-60 * 60 * 24 sinceDate:date];
        
        NSString *strdate=[formatter stringFromDate:decreaseDate];
        
        _datelabel.text=strdate;
        
        
        [self scheduleDateWiseData];
        dateIncreaseDecrease--;
        
    }
}

- (IBAction)btnAction_FutureDateArrow:(id)sender
{
    
    if (dateIncreaseDecrease <= 2)
    {
        
        NSLog(@"Print present Date %@",_datelabel.text);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd MMMM yyyy"];
        NSDate *date = [formatter dateFromString:_datelabel.text];
        
        NSDate *inceraseDate= [NSDate dateWithTimeInterval:60 * 60 * 24 sinceDate:date];
        
        NSString *strdate=[formatter stringFromDate:inceraseDate];
        
        _datelabel.text=strdate;
        [self scheduleDateWiseData];
        dateIncreaseDecrease++;
        
    }
}

- (IBAction)btnAction_ActivateDatePickerKeyboard:(id)sender
{
    
    
    //  self.activebutton=(UIButton *)sender;
    _btndatepickershow.enabled=NO;
    _btnbackdate.enabled=NO;
    _btnnextdate.enabled=NO;
    
    
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, self.view.bounds.size.width, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, self.view.bounds.size.width, 216);
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44,self.view.bounds.size.width, 216)];
    datePicker.tag = 10;
    datePicker.backgroundColor=[UIColor whiteColor];
    
    // if(self.activebutton.tag==20)
    // {
    datePicker.datePickerMode=UIDatePickerModeDate;
    // new code
    if(![_datelabel.text isEqualToString:@"Date Taken"])
    {
        // Convert string to date object
        //            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        //            [dateFormat setDateFormat:@"dd-MM-yyyy"];
        //            NSDate *date = [dateFormat dateFromString:_datelabel.text];
        //            datePicker.date=date;
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd MMMM yyyy"];
        NSDate *date = [formatter dateFromString:_datelabel.text];
        datePicker.date=date;
    }
    
    
    [self.view addSubview:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleDefault;
    toolBar.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    UIBarButtonItem *Cancel=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissDatePicker:)];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(changeDate:)];
    [toolBar setItems:[NSArray arrayWithObjects:Cancel,spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    [UIView commitAnimations];
    
}

- (void)changeDate:(id)sender
{
    dateIncreaseDecrease=0;
    NSDateFormatter *format=[[NSDateFormatter alloc]init];
    [format setDateFormat:@"dd MMMM yyyy"];
    _datelabel.text=[NSString stringWithFormat:@"%@",[format stringFromDate:datePicker.date ]];
    NSLog(@"New date : %@",_datelabel.text);
    [self scheduleDateWiseData];
    [self dismissDatePicker:nil];
    
}
- (void)dismissDatePicker:(id)sender
{
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, self.view.bounds.size.width, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}
- (void)removeViews:(id)object
{
    _btndatepickershow.enabled=YES;
    _btnbackdate.enabled=YES;
    _btnnextdate.enabled=YES;
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
    
}



@end
