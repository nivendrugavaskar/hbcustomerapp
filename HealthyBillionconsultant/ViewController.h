//
//  ViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 05/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Connection.h"

@interface ViewController : UIViewController<UITextFieldDelegate,connectionprotocol>
{
    CGRect screenBounds,storerect;
    CGPoint pointtrack,pointtrackcontentoffset;
}

@property (strong, nonatomic) IBOutlet UITextField *txtphoneno;
@property(weak,nonatomic)UITextField *activeTextField;
- (IBAction)didTapAccessKey:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *inboxLabel;
@property (strong, nonatomic) IBOutlet UIButton *btnaccesskey;
@end

