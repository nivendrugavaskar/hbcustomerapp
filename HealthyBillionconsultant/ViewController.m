//
//  ViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 05/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    UIView *paddingView;
    AppDelegate *app;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    // Do any additional setup after loading the view, typically from a nib.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    pointtrackcontentoffset=self.view.frame.origin;
    storerect=self.view.frame;
    screenBounds = [[UIScreen mainScreen] bounds];
#pragma add padding
    paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtphoneno.leftView = paddingView;
    _txtphoneno.leftViewMode = UITextFieldViewModeAlways;
#pragma hide label initially
    _inboxLabel.hidden=YES;

    
}
#pragma textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack =textField.frame.origin;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    
#pragma add toolbar
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 1000;
    toolBar.barTintColor=[UIColor colorWithRed:30/255.0f green:87/255.0f blue:140/255.0f alpha:1.0f];
    toolBar.tintColor=[UIColor whiteColor];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismisskeyboard)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [_txtphoneno setInputAccessoryView:toolBar];
    
#pragma Animate View
    CGRect rect=storerect;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(screenBounds.size.height==480)
    {
        rect.origin.y-=pointtrack.y-180;
    }
    else if(screenBounds.size.height==568)
    {
        rect.origin.y-=pointtrack.y-220;
    }
    else if(screenBounds.size.height==667)
    {
         rect.origin.y-=20;
    }
    else if(screenBounds.size.height==736)
    {
        rect.origin.y-=20;
    }
    else
    {
       rect.origin.y-=pointtrack.y-180;
    }
    self.view.frame=rect;
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self dismisskeyboard];
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if([textField.text length]>12 && range.length == 0)
    {
        return NO;
    }
    return YES;
}

- (IBAction)didTapAccessKey:(id)sender
{
    [self dismisskeyboard];
#pragma Manage buttons action
    if([_btnaccesskey.titleLabel.text isEqualToString:@"Submit"])
    {
        if([NSString validation:_txtphoneno.text])
        {
            app->customeraccesskey=_txtphoneno.text;
            Connection *instance=[[Connection alloc] init];
            instance.connectionDelegate=self;
            [instance tokenfetch];
            
            // [self webserviceaccespage1stone];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Phone number mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self.activeTextField resignFirstResponder];
        
        //[self webserviceaccespage1stone];
        
    }
    else
    {
#pragma calling service after enter phone number
        if([NSString validation:_txtphoneno.text])
        {
            [self webservicephoneenter];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Phone number mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        [self.activeTextField resignFirstResponder];
    }
    
}
#pragma connection delegate method
-(void)sucessdone:(NSDictionary *)connectiondict
{
    
    if(!connectiondict)
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        return;
    }
    [SVProgressHUD dismiss];
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    //app->customersessionname=[[dict valueForKey:@"result"] valueForKey:@"sessionName"];
    [self performSegueWithIdentifier:@"pushTopassword" sender:self];
}

//-(void)methodCall{
//    
//    
//    
//    [SVProgressHUD dismiss];
//    
//      [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//    
//    [self performSegueWithIdentifier:@"pushTopassword" sender:self];
//    
//    
//}

-(void)dismisskeyboard
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
    //[[self.view viewWithTag:1000] removeFromSuperview];
    [self.activeTextField resignFirstResponder];
}

#pragma Webservice for phone number authenticating
-(void)webservicephoneenter
{
    
   // NSString *defaultparamter=@"CUS";
    
   // _txtphoneno.text
    
    NSString *path =[NSString stringWithFormat:@"%@getaccesskey",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"CUS%@",_txtphoneno.text],@"phone",@"H6",@"user_type",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Authenticating.."];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
        // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Error" message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             _txtphoneno.text=@"";
             
         }
         else
         {
             _txtphoneno.text=@"";
             _inboxLabel.hidden=NO;
             _txtphoneno.placeholder=@"Enter Access Key";
             [_btnaccesskey setTitle:@"Submit" forState:UIControlStateNormal];
#pragma store username
             app->customerusername=[[dict valueForKey:@"result"] valueForKey:@"username"];
             
             NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
             [def setValue:app->customerusername forKey:@"customeruserName1"];
             [def synchronize];
             
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
