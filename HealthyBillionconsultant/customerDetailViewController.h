//
//  customerDetailViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 14/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HBTabbarController.h"

@interface customerDetailViewController : UIViewController
{
    AppDelegate *app;
}


@property (weak, nonatomic) IBOutlet UITextView *txtView_EmailId;
@property (weak, nonatomic) IBOutlet UITextView *txtView_PhoneNo;

@property (strong, nonatomic) IBOutlet UIScrollView *aScrollview;
@property (strong, nonatomic) IBOutlet UIView *imgviewinnerview;
@property (strong, nonatomic) IBOutlet UIImageView *profileimg;
@property (strong, nonatomic) IBOutlet UILabel *addresslabel;
@property (strong, nonatomic) IBOutlet UITextField *nametext;
@property (strong, nonatomic) IBOutlet UITextField *phntxt;
@property (strong, nonatomic) IBOutlet UITextField *emailtxt;
@property (strong, nonatomic) IBOutlet UITextView *addresstxtview;

@property(nonatomic,strong)NSMutableArray *dataArray;
@property HBTabbarController *tabbarobj;
@property (strong, nonatomic) IBOutlet UIView *viewdescription;
@property (strong, nonatomic) IBOutlet UILabel *descriptiontextlabel;


@end
