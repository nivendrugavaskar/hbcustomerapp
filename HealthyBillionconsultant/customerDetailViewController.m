//
//  customerDetailViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 14/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "customerDetailViewController.h"


@interface customerDetailViewController ()

@end

@implementation customerDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   self.title=@"CONSULTANT DETAILS";
  
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma accesing tabbar objects
     self.tabbarobj =(HBTabbarController *)self.tabBarController;
    // set text view frame
  //  _addresstxtview.frame=CGRectMake(_addresstxtview.frame.origin.x,_addresslabel.frame.origin.y-2, _addresstxtview.frame.size.width, _addresstxtview.frame.size.height);
    
    _profileimg.layer.cornerRadius=_profileimg.frame.size.width/2;
    _profileimg.layer.borderColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0].CGColor;
    _profileimg.layer.borderWidth=2.0;

    
    if(self.tabbarobj.sendarrayfromtab)
    {
        [self loadprofiledata];
    }
}
#pragma set data
-(void)loadprofiledata
{
    @try
    {
        //_nametext.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"accountname"];
        
        _nametext.text=[NSString stringWithFormat:@"%@ %@",[self.tabbarobj.sendarrayfromtab valueForKey:@"consultantsname"],[self.tabbarobj.sendarrayfromtab valueForKey:@"lastname"]];
        
      //  _phntxt.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"consultant_mobile_number"];
       // _emailtxt.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"email"];
        
        
    _txtView_PhoneNo.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"consultant_mobile_number"];
        
    _txtView_EmailId.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"email"];
        
        //_addresstxtview.text=[NSString stringWithFormat:@"%@, %@",[self.tabbarobj.sendarrayfromtab valueForKey:@"account_billing_state"],[self.tabbarobj.sendarrayfromtab valueForKey:@"account_billing_country"]];
        
        _addresstxtview.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"address"];
        
        
        
        NSString *imagepath=[self.tabbarobj.sendarrayfromtab valueForKey:@"user_image_url"];
        __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.color=[UIColor redColor];
        activityIndicator.center = CGPointMake(CGRectGetMidX(_profileimg.bounds), CGRectGetMidY(_profileimg.bounds));
        activityIndicator.hidesWhenStopped = YES;
        
        __block BOOL successfetchimage=FALSE;
        __weak UIImageView *setimage = _profileimg;
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
        [_profileimg setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"profile_img1.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             setimage.contentMode=UIViewContentModeScaleAspectFill;
             setimage.image=image;
             // new code
             
             successfetchimage=TRUE;
             [activityIndicator removeFromSuperview];
         }
                                        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             [activityIndicator removeFromSuperview];
         }];
        if(!successfetchimage)
        {
            if(![imagepath isEqualToString:@""]||imagepath)
            {
                [_profileimg addSubview:activityIndicator];
                [activityIndicator startAnimating];
            }
        }
        // new code
        if([[self.tabbarobj.sendarrayfromtab valueForKey:@"details"]isEqualToString:@""])
        {
            _viewdescription.hidden=YES;
        }
        else
        {
           _viewdescription.hidden=NO;
        }
        _descriptiontextlabel.text=[self.tabbarobj.sendarrayfromtab valueForKey:@"details"];
         CGSize newget=[self dyanamiclabel:_descriptiontextlabel];
         _viewdescription.frame=CGRectMake(_viewdescription.frame.origin.x, _viewdescription.frame.origin.y, _viewdescription.frame.size.width, 42+newget.height);
         _descriptiontextlabel.frame=CGRectMake(_descriptiontextlabel.frame.origin.x, _descriptiontextlabel.frame.origin.y, _descriptiontextlabel.frame.size.width,newget.height);
       
        if((_viewdescription.frame.origin.y+_viewdescription.frame.size.height)>_aScrollview.frame.size.height)
        {
             _aScrollview.contentSize=CGSizeMake(_aScrollview.contentSize.width,_aScrollview.frame.size.height+((_viewdescription.frame.origin.y+_viewdescription.frame.size.height)-_aScrollview.frame.size.height)+20);
        }
       
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"nill value");
    }
    
    
}
// new method for dynamic height
-(CGSize)dyanamiclabel:(UILabel *)takelabel
{
    
    CGSize constrainedSize = CGSizeMake(takelabel.frame.size.width  , MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          takelabel.font, NSFontAttributeName,
                                          nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:takelabel.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = takelabel.frame;
    newFrame.size.height = requiredHeight.size.height;
    takelabel.frame = newFrame;
    return takelabel.frame.size;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
