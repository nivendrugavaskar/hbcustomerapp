//
//  customerViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 13/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "customerViewController.h"


@interface customerViewController ()
{
    float cellheight;
    NSMutableArray *customerarray;
    NSDictionary *dict;
    NSInteger pagenumber;
    BOOL enddata;
    
    NSIndexPath *globalindex;
}

@end

@implementation customerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setCustomNavigationButton:self];
    self.title=@"CONSULTANTS";
    
     app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    enddata=FALSE;
    pagenumber=0;
    [self.navigationController.navigationBar setCustomNavigationButton:self];
#pragma cell height
    cellheight=50.0;
    customerarray=[[NSMutableArray alloc] init];
    
//    GlobalService *obj=[[GlobalService alloc]init];
//    obj.delegate=self;
//    [obj webserviceGetToken];

    
    
    
    
    // Global dictionary
  //  NSLog(@"Dict=%@",app->globalcustomerdetail);
    
     [self customerfetchdetailservice];
    
    
    // new code
    
    
}



-(void)restructtable
{
    self.aTableview.layer.cornerRadius=5.0;
    if([customerarray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
    {
        self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, [customerarray count]*cellheight);
    }
    else
    {
        self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, self.view.frame.size.height-self.aTableview.frame.origin.y-5);
    }
    self.aTableview.layer.borderWidth=1.0;
    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    
//#pragma maintain scrroll view
//    if([customerarray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
//    {
//        self.aTableview.scrollEnabled=NO;
//    }
//    else
//    {
//        self.aTableview.scrollEnabled=YES;
//    }
}

-(void)customerfetchdetailservice
{
//Api: "http://dev1.businessprodemo.com/hbcr/php/webservice.php?operation=customer_consultants";
//    
//    METHOD_TYPE_GET
//    keys = { "customerid","sessionName","limit_from","limit_to"};
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
     [SVProgressHUD showWithStatus:@"Please wait.."];
    
    NSString *path =[NSString stringWithFormat:@"%@customer_consultants",app->parentUrl];
   // NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"consultantid"],@"consultantid",[NSString stringWithFormat:@"%d",(int)(pagenumber*10)],@"limit_from",@"10",@"limit_to",nil];
    
     NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",[app->globalcustomerdetail valueForKeyPath:@"data.Account.accountid"],@"customerid",[NSString stringWithFormat:@"%d",(int)(pagenumber*10)],@"limit_from",@"10",@"limit_to",nil];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
   // [SVProgressHUD showWithStatus:@"Loading.."];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         dict=[[NSDictionary alloc] init];
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
           
             
         }
         else
         {
             if([[dict valueForKey:@"result"] valueForKey:@"data"]==nil||[[[dict valueForKey:@"result"] valueForKey:@"data"]isKindOfClass:[NSNull class]])
             {
//                 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//                 [SVProgressHUD dismiss];
                 pagenumber--;
                 enddata=TRUE;
             }
             else
             {
                 enddata=FALSE;
             NSMutableArray *localarray=[[[dict valueForKey:@"result"] valueForKey:@"data"] mutableCopy];
             if([localarray count]>0)
             {
             for(int i=0;i<[localarray count];i++)
             {
                 [customerarray addObject:[localarray objectAtIndex:i]];
             }
             }
             [self restructtable];
             [self.aTableview reloadData];
             }
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}

//// new code for pagination
//-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
//                    withVelocity:(CGPoint)velocity
//             targetContentOffset:(inout CGPoint*)targetContentOffset
//{
//    //Intercept and recalculate the desired content offset
//    CGPoint targetOffset = [self recalculateUsingTargetContentOffset:targetContentOffset];
//    
//    //Reset the targetContentOffset with your recalculated value
//    targetContentOffset->y = targetOffset.y;
//}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
   
    if(scrollView.contentOffset.y+scrollView.frame.size.height==scrollView.contentSize.height)
    {
        if(!enddata)
        {
        NSLog(@"call service here");
        pagenumber++;
        [self customerfetchdetailservice];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    customerTableViewCell *cell=(customerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    
    
    if(cell==nil)
    {
        cell=[[customerTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    @try
    {
        cell.phnnolabel.frame=CGRectMake(self.aTableview.frame.size.width-130,cell.phnnolabel.frame.origin.y, cell.phnnolabel.frame.size.width,cell.phnnolabel.frame.size.height);
        cell.divider.frame=CGRectMake(0, cell.divider.frame.origin.y, self.aTableview.frame.size.width, cell.divider.frame.size.height);
        cell.arraoimageview.frame=CGRectMake(self.aTableview.frame.size.width-25, cell.arraoimageview.frame.origin.y, cell.arraoimageview.frame.size.width, cell.arraoimageview.frame.size.height);
        //
        cell.namelabel.text=[[customerarray objectAtIndex:indexPath.row] valueForKey:@"consultantsname"];
       cell.phnnolabel.text=[[customerarray objectAtIndex:indexPath.row] valueForKey:@"consultant_mobile_number"];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellheight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [customerarray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSLog(@"indexpathrow=%ld",(long)indexPath.row);
     globalindex=indexPath;
     [self performSegueWithIdentifier:@"pushtocustomerdetails" sender:globalindex];
//    // service call
//    [self sessionforparticularcustomer];
    
}
//#pragma session for particular customer
//-(void)sessionforparticularcustomer
//{
//    
//    NSString *path =[NSString stringWithFormat:@"%@consultant_customers",app->parentUrl];
//    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",[[[app->globalcustomerdetail valueForKey:@"data"] valueForKey:@"User"] valueForKey:@"consultantid"],@"consultantid",[NSString stringWithFormat:@"%d",(int)(pagenumber*10)],@"limit_from",@"10",@"limit_to",nil];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
//    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
//    [SVProgressHUD showWithStatus:@"Loading.."];
//    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
//     {
//         
//         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
//         NSLog(@"%@",responseStr);
//         dict=[[NSDictionary alloc] init];
//         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
//         // NSLog(@"%@",dict);
//         //here is place for code executed in success case
//         if([[dict valueForKey:@"success"]integerValue]==0)
//         {
//             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//             [alert show];
//             
//             
//         }
//         else
//         {
//            [self performSegueWithIdentifier:@"pushtocustomerdetails" sender:indexPath];
//         }
//         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//         [SVProgressHUD dismiss];
//         
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
//         
//         //here is place for code executed in success case
//         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
//         [SVProgressHUD dismiss];
//         NSLog(@"Error: %@", [error localizedDescription]);
//         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//         [alert show];
//     }];
//    
//}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"pushtocustomerdetails"])
    {
        HBTabbarController *instance = (HBTabbarController *)segue.destinationViewController;
        if([sender isKindOfClass:[NSIndexPath class]])
        {
            NSIndexPath *indexPath = (NSIndexPath *)sender;
            NSLog(@"row=%ld",(long)indexPath.row);
            NSLog(@"section=%ld",(long)indexPath.section);
//            instance.delegate=self;
//            instance.sendindex=(int)indexPath.row;
            instance.sendarrayfromtab=[customerarray objectAtIndex:indexPath.row];
            instance.customizableViewControllers=nil;
        }
        else
        {
        
        }
        
    }
}


@end
