//
//  invoiceDetailsViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 19/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface invoiceDetailsViewController : UIViewController
{
    AppDelegate *app;
    
}
@property (strong, nonatomic) IBOutlet UILabel *invoicename;
@property (strong, nonatomic) IBOutlet UILabel *invoicedate;
@property (strong, nonatomic) IBOutlet UIImageView *istline;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *namevalue;
@property (strong, nonatomic) IBOutlet UILabel *phnlabel;
@property (strong, nonatomic) IBOutlet UILabel *phnvalue;
@property (strong, nonatomic) IBOutlet UIImageView *secondimage;
@property (strong, nonatomic) IBOutlet UITableView *aTableview;
@property (strong, nonatomic) IBOutlet UIImageView *thiedlineimage;

@property (strong, nonatomic) IBOutlet UILabel *serialnolabel;
@property (strong, nonatomic) IBOutlet UILabel *servicenamelabel;
@property (strong, nonatomic) IBOutlet UILabel *unitpricelabel;
@property (strong, nonatomic) IBOutlet UILabel *unitlabel;
@property (strong, nonatomic) IBOutlet UILabel *discountlabel;
@property (strong, nonatomic) IBOutlet UILabel *subtotallabel;

@property (weak, nonatomic) IBOutlet UILabel *totalsessionheldLabel;

@property (weak, nonatomic) IBOutlet UILabel *heldSesssion;


@property(strong,nonatomic)NSMutableArray *invoicedetailsarray;

@end
