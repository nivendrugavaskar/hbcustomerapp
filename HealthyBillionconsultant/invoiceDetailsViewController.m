//
//  invoiceDetailsViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 19/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "invoiceDetailsViewController.h"
#import "invoicelistTableViewCell.h"

@interface invoiceDetailsViewController ()
{
    NSMutableArray *itemarray;
    float cellheight;
    NSDictionary *dict;
}

@end

@implementation invoiceDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    self.title=@"INVOICE DETAILS";
    cellheight=44.0f;
   // itemarray=[[NSMutableArray alloc] initWithObjects:@"Item1",@"Item2",@"Item3",@"Item4",@"Item5",@"Item6",@"Item7",@"Item8", nil];
    
    self.navigationItem.hidesBackButton=TRUE;
    
    // add back button
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    backbarbutton.style = UIBarButtonItemStylePlain;
    self.navigationItem.leftBarButtonItems = @[backbarbutton];
    
    itemarray=[[NSMutableArray alloc]init];
#pragma service call
    if([self.invoicedetailsarray count]>0)
    {
        _namevalue.text=[NSString stringWithFormat:@"%@ %@",[app->globalcustomerdetail valueForKeyPath:@"data.User.first_name"],[app->globalcustomerdetail valueForKeyPath:@"data.User.last_name"]];
        _phnvalue.text=[app->globalcustomerdetail valueForKeyPath:@"data.User.phone_mobile"];
        
        _invoicename.text=[NSString stringWithFormat:@"Invoice %@",[self.invoicedetailsarray valueForKey:@"invoice_no"]];
        
        _totalsessionheldLabel.text=[self.invoicedetailsarray valueForKey:@"total_sessions"];
        
        _heldSesssion.text=[self.invoicedetailsarray valueForKey:@"total_held"];
        
        
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"dd/MM/yyyy"];
        NSString *stringdate=[self.invoicedetailsarray valueForKey:@"invoicedate"];
        NSDate *gotdate=[formatter dateFromString:stringdate];
        NSString *strDate = [formatter2 stringFromDate:gotdate];
        _invoicedate.text=strDate;
        [self servicecallinvoicedetails];
    }
    
    
    
    
}
//#pragma servicecall invoice detail
//-(void)restructtable
//{
//    self.aTableview.layer.cornerRadius=5.0;
//    self.aTableview.layer.borderWidth=1.0;
//    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
//}
-(void)servicecallinvoicedetails
{
    
    //http://dev1.businessprodemo.com/hbcrm/php/webservice.php?operation=getInvoiceDetails&invoiceid=1280
    
    NSString *path =[NSString stringWithFormat:@"%@getInvoiceDetails",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[_invoicedetailsarray valueForKey:@"invoiceid"],@"invoiceid",app->customersessionname,@"sessionName",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Loading.."];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         dict=[[NSDictionary alloc] init];
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
             
         }
         else
         {
                // itemarray=[[dict valueForKey:@"result"] mutableCopy];
             
             itemarray=[[dict valueForKeyPath:@"result.invoice_details"]mutableCopy];
             
             
             
             
             
                 //[self restructtable];
                 [self.aTableview reloadData];
             
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellid=@"Cell";
    invoicelistTableViewCell *cell=(invoicelistTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    
    // set text colour and seperator colour
    //    cell.foodName.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    //    cell.date.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    //    cell.seperatorlabel.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    if(cell==nil)
    {
        cell=[[invoicelistTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    @try
    {
#pragma setting frame resizing
        cell.serialnovalue.frame=CGRectMake(self.serialnolabel.frame.origin.x-2,cell.serialnovalue.frame.origin.y, cell.serialnovalue.frame.size.width,cell.serialnovalue.frame.size.height);
        cell.itemname.frame=CGRectMake(22,cell.itemname.frame.origin.y, cell.itemname.frame.size.width-7,cell.itemname.frame.size.height);
        cell.unitpricevalue.frame=CGRectMake(self.unitpricelabel.frame.origin.x-2,cell.unitpricevalue.frame.origin.y, cell.unitpricevalue.frame.size.width,cell.unitpricevalue.frame.size.height);
        cell.unitvalue.frame=CGRectMake(self.unitlabel.frame.origin.x-2,cell.unitvalue.frame.origin.y, cell.unitvalue.frame.size.width,cell.unitvalue.frame.size.height);
        cell.discountvalue.frame=CGRectMake(self.discountlabel.frame.origin.x-2,cell.discountvalue.frame.origin.y, cell.discountvalue.frame.size.width,cell.discountvalue.frame.size.height);
        cell.subtotalvalue.frame=CGRectMake(self.subtotallabel.frame.origin.x-2,cell.subtotalvalue.frame.origin.y, cell.subtotalvalue.frame.size.width,cell.subtotalvalue.frame.size.height);
        
#pragma setvalues
        cell.serialnovalue.text=[NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
        cell.itemname.text=[[itemarray objectAtIndex:indexPath.row] valueForKey:@"servicename"];
        cell.unitpricevalue.text=[[itemarray objectAtIndex:indexPath.row] valueForKey:@"unitprice"];
        cell.unitvalue.text=[[itemarray objectAtIndex:indexPath.row] valueForKey:@"quantity"];
        cell.discountvalue.text=[[itemarray objectAtIndex:indexPath.row] valueForKey:@"discount_amount"];
        float subtotalamount=([[[itemarray objectAtIndex:indexPath.row] valueForKey:@"unitprice"] floatValue]*[[[itemarray objectAtIndex:indexPath.row] valueForKey:@"quantity"] floatValue])-[[[itemarray objectAtIndex:indexPath.row] valueForKey:@"discount_amount"] floatValue];
        cell.subtotalvalue.text=[NSString stringWithFormat:@"%0.2f",subtotalamount];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellheight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [itemarray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
}

- (UIView *)  tableView:(UITableView *)tableView
 viewForFooterInSection:(NSInteger)section{
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.aTableview.frame.size.width, 30)];
    footerView.backgroundColor=[UIColor colorWithRed:217/255.0f green:245/255.0f blue:250/255.0f alpha:1.0f];
    footerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UILabel *total = nil;
    UIImageView *imgview=nil;
   
    if ([tableView isEqual:self.aTableview] &&
        section == 0)
    {
        if([itemarray count]>0)
        {
        imgview=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, self.aTableview.frame.size.width, 2)];
        imgview.backgroundColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0f];
        [footerView addSubview:imgview];
        
        
        total = [[UILabel alloc] initWithFrame:CGRectMake(2,3, 100, 17)];
        total.font=[UIFont boldSystemFontOfSize:14.0f];
        total.textColor=[UIColor blackColor];
        total.text = @"Total-Amount";
        total.backgroundColor = [UIColor clearColor];
        //[result sizeToFit];
        [footerView addSubview:total];
        
        total = [[UILabel alloc] initWithFrame:CGRectMake(self.aTableview.frame.size.width-148,3, 140, 17)];
        total.textAlignment=NSTextAlignmentRight;
        total.font=[UIFont boldSystemFontOfSize:14.0f];
        total.textColor=[UIColor blackColor];
        total.text = [self.invoicedetailsarray valueForKey:@"total_amount"];
        total.backgroundColor = [UIColor clearColor];
        //[result sizeToFit];
        [footerView addSubview:total];
        
        total = [[UILabel alloc] initWithFrame:CGRectMake(2,20, 100, 17)];
        total.font=[UIFont systemFontOfSize:14.0f];
        total.textColor=[UIColor blackColor];
        total.text = @"Paid Amount";
        total.backgroundColor = [UIColor clearColor];
        //[result sizeToFit];
        [footerView addSubview:total];
        
        total = [[UILabel alloc] initWithFrame:CGRectMake(self.aTableview.frame.size.width-148,20, 140, 17)];
        total.textAlignment=NSTextAlignmentRight;
        total.font=[UIFont systemFontOfSize:14.0f];
        total.textColor=[UIColor blackColor];
        total.text = [self.invoicedetailsarray valueForKey:@"payment_amount"];
        total.backgroundColor = [UIColor clearColor];
        //[result sizeToFit];
        [footerView addSubview:total];
        
        
        
        total = [[UILabel alloc] initWithFrame:CGRectMake(2,37, 100, 17)];
        total.font=[UIFont boldSystemFontOfSize:14.0f];
        if([[self.invoicedetailsarray valueForKey:@"due_amount"] floatValue]<0)
        {
            total.textColor=[UIColor colorWithRed:25/255.0f green:85/255.0f blue:3/255.0f alpha:1.0f];
            total.text = @"Refund";
        }
        else
        {
            total.textColor=[UIColor colorWithRed:243/255.0f green:10/255.0f blue:29/255.0f alpha:1.0f];
            total.text = @"Due";
        }
        
        total.backgroundColor = [UIColor clearColor];
        //[result sizeToFit];
        [footerView addSubview:total];
        
        total = [[UILabel alloc] initWithFrame:CGRectMake(self.aTableview.frame.size.width-148,37, 140, 17)];
        total.textAlignment=NSTextAlignmentRight;
        total.font=[UIFont systemFontOfSize:14.0f];
        if([[self.invoicedetailsarray valueForKey:@"due_amount"] floatValue]<0)
        {
            total.textColor=[UIColor colorWithRed:25/255.0f green:85/255.0f blue:3/255.0f alpha:1.0f];
            float positive=-[[self.invoicedetailsarray valueForKey:@"due_amount"] floatValue];
            total.text = [NSString stringWithFormat:@"%0.2f",positive];
        }
        else
        {
            total.textColor=[UIColor colorWithRed:243/255.0f green:10/255.0f blue:29/255.0f alpha:1.0f];
            total.text = [self.invoicedetailsarray valueForKey:@"due_amount"];
        }
        
        total.backgroundColor = [UIColor clearColor];
        //[result sizeToFit];
        [footerView addSubview:total];
        
        imgview=[[UIImageView alloc] initWithFrame:CGRectMake(0,58, self.aTableview.frame.size.width, 2)];
        imgview.backgroundColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0f];
        [footerView addSubview:imgview];
        
    }
    }
    
    return footerView;
    
}

@end
