//
//  invoicesTableViewCell.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface invoicesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *invoicename;
@property (strong, nonatomic) IBOutlet UILabel *divider;
@property (strong, nonatomic) IBOutlet UILabel *totalamount;
@property (strong, nonatomic) IBOutlet UILabel *paidamount;
@property (strong, nonatomic) IBOutlet UILabel *dueamount;
@property (strong, nonatomic) IBOutlet UILabel *duelabel;

@property (weak, nonatomic) IBOutlet UILabel *totalsessionLabel;
@property (weak, nonatomic) IBOutlet UILabel *heldLabel;

@end
