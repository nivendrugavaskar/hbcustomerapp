//
//  invoicesViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "invoicesTableViewCell.h"
#import "invoiceDetailsViewController.h"


@interface invoicesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITableView *aTableview;
@property (strong, nonatomic) IBOutlet UIView *innerview;

@end
