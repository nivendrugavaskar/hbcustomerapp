//
//  invoicesViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "invoicesViewController.h"

@interface invoicesViewController ()
{
    float cellheight;
    NSMutableArray *invoicearray;
    NSDictionary *dict;
    NSInteger pagenumber;
    BOOL enddata;
    
    NSString *customerid;
}

@end

@implementation invoicesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
   // NSLog(@"global dict=%@",app->globalcustomerdetail);
    enddata=FALSE;
    pagenumber=0;
    
    customerid=[[NSString alloc] init];
    customerid=[app->globalcustomerdetail valueForKeyPath:@"data.Consultant.userid"];
#pragma cell height
    cellheight=125.0;
    cellheight=174.0;
    invoicearray=[[NSMutableArray alloc] init];
   // invoicearray=[[NSMutableArray alloc] initWithObjects:@"Invoice 1",@"Invoice 2",@"Invoice 3",@"Invoice 4",@"Invoice 5",@"Invoice 6", nil];
    
#pragma service for invoice details
    
  
 [self invoicefetchdetailservice];
    
    
   
    
  // [self restructtable];
}


-(void)restructtable
{
    self.aTableview.layer.cornerRadius=5.0;
    if([invoicearray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
    {
        self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, [invoicearray count]*cellheight);
    }
    else
    {
        self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, self.view.frame.size.height-self.aTableview.frame.origin.y-5);
    }
    self.aTableview.layer.borderWidth=1.0;
    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    
#pragma maintain scrroll view
    if([invoicearray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
    {
        self.aTableview.scrollEnabled=NO;
    }
    else
    {
        self.aTableview.scrollEnabled=YES;
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
//    if(scrollView.contentOffset.y+scrollView.frame.size.height==scrollView.contentSize.height)
//    {
//        if(!enddata)
//        {
//            NSLog(@"call service here");
//            pagenumber++;
//            [self invoicefetchdetailservice];
//        }
//    }
}
-(void)invoicefetchdetailservice
{
    
    
    // For checking now
  //  customerid=@"984";
   // customerid=@"2208";
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    NSString *path =[NSString stringWithFormat:@"%@getConsumerInvoiceDetails",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[app->globalcustomerdetail valueForKeyPath:@"data.Account.accountid"],@"customer_id",[NSString stringWithFormat:@"%d",(int)(pagenumber*10)],@"limit_from",@"200",@"limit_to",app->customersessionname,@"sessionName",nil];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
   // [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
   // [SVProgressHUD showWithStatus:@"Loading.."];
    [manager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         dict=[[NSDictionary alloc] init];
         dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
             
         }
         else
         {
             if([[dict valueForKey:@"result"] valueForKey:@"data"]==nil||[[[dict valueForKey:@"result"] valueForKey:@"data"]isKindOfClass:[NSNull class]])
             {
                 //                 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 //                 [SVProgressHUD dismiss];
                 pagenumber--;
                 enddata=TRUE;
             }
             else
             {
                 enddata=FALSE;
                 NSMutableArray *localarray=[[dict valueForKey:@"result"] mutableCopy];
                 if([localarray count]>0)
                 {
                     for(int i=0;i<[localarray count];i++)
                     {
                         [invoicearray addObject:[localarray objectAtIndex:i]];
                     }
                 }
                 [self restructtable];
                 [self.aTableview reloadData];
             }
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"INVOICES";
    [self setCustomNavigationButton];
    
    
}

#pragma cutomize navigation
-(void)setCustomNavigationButton
{
    
     [self.navigationController.navigationBar setCustomNavigationButton:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"pushtoinvoicedetail"])
    {
        invoiceDetailsViewController *instance = (invoiceDetailsViewController *)segue.destinationViewController;
        if([sender isKindOfClass:[NSIndexPath class]])
        {
            NSIndexPath *indexPath = (NSIndexPath *)sender;
            instance.invoicedetailsarray=[invoicearray objectAtIndex:indexPath.row];
        }
    }
}


#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *cellid=@"Cell";
    invoicesTableViewCell *cell=(invoicesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellid];
    //    // new
    //    cell.delegate=(id)self;
    
    // set text colour and seperator colour
//    cell.foodName.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
//    cell.date.textColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
//    cell.seperatorlabel.backgroundColor=[UIColor colorWithRed:188/255.0f green:188/255.0f blue:188/255.0f alpha:1.0f];
    
    if(cell==nil)
    {
        cell=[[invoicesTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellid];
    }
    
    @try
    {
        cell.divider.frame=CGRectMake(0, cell.divider.frame.origin.y, self.aTableview.frame.size.width, cell.divider.frame.size.height);
        cell.invoicename.frame=CGRectMake(70, cell.invoicename.frame.origin.y, self.aTableview.frame.size.width-75, cell.invoicename.frame.size.height);
        cell.totalamount.frame=CGRectMake(self.aTableview.frame.size.width-185, cell.totalamount.frame.origin.y, cell.totalamount.frame.size.width, cell.totalamount.frame.size.height);
        cell.paidamount.frame=CGRectMake(self.aTableview.frame.size.width-185, cell.paidamount.frame.origin.y, cell.paidamount.frame.size.width, cell.paidamount.frame.size.height);
        cell.totalsessionLabel.frame=CGRectMake(self.aTableview.frame.size.width-185, cell.totalsessionLabel.frame.origin.y, cell.totalsessionLabel.frame.size.width, cell.totalsessionLabel.frame.size.height);
        cell.heldLabel.frame=CGRectMake(self.aTableview.frame.size.width-185, cell.heldLabel.frame.origin.y, cell.heldLabel.frame.size.width, cell.heldLabel.frame.size.height);
        cell.dueamount.frame=CGRectMake(self.aTableview.frame.size.width-185, cell.dueamount.frame.origin.y, cell.dueamount.frame.size.width, cell.dueamount.frame.size.height);
       
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        [formatter2 setDateFormat:@"dd MMMM yyyy"];
        NSString *stringdate=[[invoicearray objectAtIndex:indexPath.row] valueForKey:@"invoicedate"];
        NSDate *gotdate=[formatter dateFromString:stringdate];
        NSString *strDate = [formatter2 stringFromDate:gotdate];
        // new code
//        cell.invoicename.marqueeType = MLLeftRight;
//        cell.invoicename.rate = 30.0f;
//        cell.invoicename.fadeLength = 10.0f;
//        cell.invoicename.leadingBuffer = 30.0f;
//        cell.invoicename.trailingBuffer = 20.0f;
//        cell.invoicename.textAlignment = NSTextAlignmentCenter;
        cell.invoicename.text=[NSString stringWithFormat:@"%@, %@",[[invoicearray objectAtIndex:indexPath.row] valueForKey:@"invoice_no"],strDate];
        cell.totalamount.text=[NSString stringWithFormat:@"%@",[[invoicearray objectAtIndex:indexPath.row] valueForKey:@"total_amount"]];
        cell.paidamount.text=[NSString stringWithFormat:@"%@",[[invoicearray objectAtIndex:indexPath.row] valueForKey:@"payment_amount"]];
        cell.dueamount.text=[NSString stringWithFormat:@"%@",[[invoicearray objectAtIndex:indexPath.row] valueForKey:@"due_amount"]];
        
         cell.totalsessionLabel.text=[NSString stringWithFormat:@"%@",[[invoicearray objectAtIndex:indexPath.row] valueForKey:@"total_sessions"]];
        
         cell.heldLabel.text=[NSString stringWithFormat:@"%@",[[invoicearray objectAtIndex:indexPath.row] valueForKey:@"total_held"]];
        
        
#pragma condition when due amount negative
        if([cell.dueamount.text floatValue]<0)
        {
            float postive=-([cell.dueamount.text floatValue]);
            cell.dueamount.text=[NSString stringWithFormat:@"%0.2f",postive];
            cell.duelabel.text=@"Refund";
            cell.duelabel.textColor=[UIColor colorWithRed:25/255.0f green:85/255.0f blue:3/255.0f alpha:1.0f];
            cell.dueamount.textColor=[UIColor colorWithRed:25/255.0f green:85/255.0f blue:3/255.0f alpha:1.0f];
        }
//        cell.date.text=[[foodListingStoreArray objectAtIndex:indexPath.row] valueForKey:@"date"];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellheight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [invoicearray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"indexpathrow=%ld",(long)indexPath.row);
    [self performSegueWithIdentifier:@"pushtoinvoicedetail" sender:indexPath];
    
}


@end
