//
//  myScheduleViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TKCalendarMonthViewController.h"
#import "TKCalendarMonthView.h"


#import "CalendarView.h"

NSMutableArray *scheduledetails;

@interface myScheduleViewController : UIViewController<TKCalendarMonthViewDataSource, TKCalendarMonthViewDelegate,CalendarDelegate>
{
    AppDelegate *app;
    TKCalendarMonthView *calendar;
    
}
@property (strong, nonatomic) IBOutlet UILabel *lbl_NoScheduleFound;

// new for tapku
@property (nonatomic,strong) CalendarView *calenderinstance;
@property(nonatomic, strong)NSArray *responseArr;
@property(nonatomic, strong)NSString *responseStr;



@end
