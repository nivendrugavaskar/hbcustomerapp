//
//  myScheduleViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//
#import <CoreGraphics/CoreGraphics.h>
#import "myScheduleViewController.h"
#import "ScheduledetailsViewController.h"


@interface myScheduleViewController ()
{
     BOOL loaddidappear;
     NSMutableArray *sendarrayafterdateclick;
     CalendarView *_sampleView;
     NSCalendar *gregorian;
}

@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) NSArray *disabledDates;


// new for tapku
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableDictionary *dataDictionary;


@end

@implementation myScheduleViewController
{
    // new for tapku
    NSMutableArray *selectedDateArr;
    NSMutableArray *sessionStatusArr;
    NSString *selectedDateStr;
     NSMutableArray *serviceDateArr;
    NSDateFormatter *df;

}

// for tapku
@synthesize responseArr, responseStr;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    _lbl_NoScheduleFound.hidden=YES;
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // for tapku
    selectedDateArr = [[NSMutableArray alloc]init];
    self.dataDictionary = [[NSMutableDictionary alloc]init];
#pragma scheduledetails array
    scheduledetails=[[NSMutableArray alloc] init];
   
    
//    [self createCalendarView];
//    [self showHideCalendarView];
#pragma service call for schedule
   // [self myschedulewebservice];
    
#pragma customcalender
    _sampleView= [[CalendarView alloc]initWithFrame:CGRectMake(0,54, self.view.bounds.size.width, self.view.bounds.size.height)];
    _sampleView.delegate = self;
    [_sampleView setBackgroundColor:[UIColor clearColor]];
    _sampleView.calendarDate = [NSDate date];
    [self.view addSubview:_sampleView];
    
    
     [self myschedulewebservice];
    
}

-(void)getmonthDetailon_Swipe:(BOOL)hiddenStaus
{
    // NSLog(@"hiddenstatus %hhd",hiddenStaus);
    if (hiddenStaus==TRUE) {
        
        _lbl_NoScheduleFound.hidden=TRUE;
    }else{
        
        _lbl_NoScheduleFound.hidden=FALSE;
    }
    
    
    
}


-(void)tappedOnDate:(NSDate *)selectedDate
{
    NSLog(@"tappedOnDate %@(GMT)",selectedDate);
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate:selectedDate];
    NSDate *globaldate=[NSDate dateWithTimeInterval: seconds sinceDate:selectedDate];
    NSLog(@"tappedOnDate1 %@(GMT)",globaldate);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSString *selecteddatestr=[dateFormatter stringFromDate:globaldate];
    NSLog(@"Local date in string=%@",selecteddatestr);
    
    sendarrayafterdateclick=[[NSMutableArray alloc] init];
   // NSString *comparedate=[NSString stringWithFormat:@"%@ 18:30:00 +0000",selecteddatestr];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *comparedate = [dateFormat dateFromString:selecteddatestr];
    
    for(int i=0;i<[selectedDateArr count];i++)
    {
        if([comparedate isEqualToDate:[selectedDateArr objectAtIndex:i]])
        {
            [sendarrayafterdateclick addObject:[scheduledetails objectAtIndex:i]];
        }
    }
    
    
    if(loaddidappear)
    {
        loaddidappear=FALSE;
        [self performSegueWithIdentifier:@"pushtoscheduledetails" sender:selectedDate];
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"MY SCHEDULE";
    [self setCustomNavigationButton];
    


   
}

#pragma cutomize navigation
-(void)setCustomNavigationButton
{
    
   [self.navigationController.navigationBar setCustomNavigationButton:self];

}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"pushtoscheduledetails"])
    {
        if([sender isKindOfClass:[NSDate class]])
        {
            NSDate *senddate=(NSDate *)sender;
            ScheduledetailsViewController *instace=(ScheduledetailsViewController *)segue.destinationViewController;
            instace.selecteddate=senddate;
            instace.tableviewArray=sendarrayafterdateclick;
        }
    }
}


#pragma  mark calendar delegate

- (void)calendarMonthView:(TKCalendarMonthView *)monthView monthDidChange:(NSDate *)d {
    
    //   [super calendarMonthView:monthView monthDidChange:d animated:YES];
    NSLog(@"calendarMonthView monthDidChange");
    
    
}
#pragma  mark - calendar data source

-(NSArray *)calendarMonthView:(TKCalendarMonthView *)monthView marksFromDate:(NSDate *)startDate toDate:(NSDate *)lastDate
{
    
    
    [self generateRandomDataForStartDate:startDate endDate:lastDate];
    return self.dataArray;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    loaddidappear=TRUE;
    
}
-(NSDate *)getLocalTimeFrom:(NSDate *)GMTTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSLog(@"%@",tz);
    NSInteger seconds = [tz secondsFromGMTForDate: GMTTime];
    NSLog(@"%ld",(long)seconds);
    return [NSDate dateWithTimeInterval: seconds sinceDate: GMTTime];
}

-(void)calendarMonthView:(TKCalendarMonthView *)monthView didSelectDate:(NSDate *)d
{
    // [self.calendarTableView reloadData];
    
    df = [[NSDateFormatter alloc]init];
    df.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-GB"];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    NSLog(@" date_Selected %@", [calendar dateSelected]);
    NSDate * calendarDate = [calendar dateSelected];
//    if ([selectedDateArr containsObject:[d description]])
//    {
        // hide now
        //[SVProgressHUD showWithStatus:@"Please wait"];
        [df  setDateFormat:@"yyyy-MM-dd"];
        
        selectedDateStr = [df stringFromDate:calendarDate];
        
        // [self performSelector:@selector(selectedDateService) withObject:nil afterDelay:0.5];
   // }
    
    
    
    
    [df  setDateFormat:@"yyyy-MM-dd"];
    
    
    // new code
    sendarrayafterdateclick=[[NSMutableArray alloc] init];
    NSString *comparedate=[NSString stringWithFormat:@"%@ 00:00:00 +0000",selectedDateStr];
    for(int i=0;i<[selectedDateArr count];i++)
    {
        if([comparedate isEqualToString:[selectedDateArr objectAtIndex:i]])
        {
            [sendarrayafterdateclick addObject:[scheduledetails objectAtIndex:i]];
        }
    }
    
    
    if(loaddidappear)
    {
    loaddidappear=FALSE;
    [self performSegueWithIdentifier:@"pushtoscheduledetails" sender:calendarDate];
    }
    
    
}


- (void) generateRandomDataForStartDate:(NSDate*)startDate endDate:(NSDate*)lastDate
{
    
    
    
    // Initialise empty marks array, this will be populated with TRUE/FALSE in order for each day a marker should be placed on.
    //	NSMutableArray *marks = [NSMutableArray array];
    self.dataArray = [NSMutableArray array];
    
    // Initialise calendar to current type and set the timezone to never have daylight saving
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    NSDateComponents *comp = [cal components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit |
                                              NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit)
                                    fromDate:startDate];
    
    NSDate *d = [cal dateFromComponents:comp];
    
    // Init offset components to increment days in the loop by one each time
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setDay:1];
    
    
    // for each date between start date and end date check if they exist in the data array
    while (YES) {
        
        // Is the date beyond the last date? If so, exit the loop.
        // NSOrderedDescending = the left value is greater than the right
        if ([d compare:lastDate] == NSOrderedDescending) {
            break;
        }
        
        // If the date is in the data array, add it to the marks array, else don't
        if ([selectedDateArr containsObject:[d description]])
        {
            
            
            
            //     (self.dataDictionary)[d] = @[@"Item one",@"Item two"];
            [self.dataArray addObject:@YES];
            
        } else {
            [self.dataArray addObject:[NSNumber numberWithBool:NO]];
        }
        
        
        
        // Increment day using offset components (ie, 1 day in this instance)
        d = [cal dateByAddingComponents:offsetComponents toDate:d options:0];
        
    }
    
    
}

#pragma webservice
-(void)myschedulewebservice
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    NSString *path =[NSString stringWithFormat:@"%@consultant_sessions",app->parentUrl];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",nil];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        // NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         
         
        // NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             
             
         }
         else
         {
             @try {
                 scheduledetails=[[[dict valueForKey:@"result"] valueForKey:@"data"] valueForKey:@"Sessions"];
                 // new code
                 selectedDateArr=[[NSMutableArray alloc]init];
                 sessionStatusArr=[[NSMutableArray alloc]init];
                 for(int i=0;i<[scheduledetails count];i++)
                 {
                   //  NSLog(@"Stringdate=%@",[[NSString stringWithFormat:@"%@",[[scheduledetails objectAtIndex:i] valueForKey:@"start_date"]] mutableCopy]);
                     NSString *datestring=[[NSString stringWithFormat:@"%@",[[scheduledetails objectAtIndex:i] valueForKey:@"start_date"]] mutableCopy];
                     
                     NSString *sessionstatus=[[[scheduledetails objectAtIndex:i]valueForKey:@"cf_941"]mutableCopy];
                     // Convert string to date object
                     NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                     [dateFormat setDateFormat:@"yyyy-MM-dd"];
                     NSDate *date = [dateFormat dateFromString:datestring];
                     [selectedDateArr addObject:date];
                     
                     [sessionStatusArr addObject:sessionstatus];
                     
                 }
                 // new code
                 
                 _sampleView->getsessionStatusArr=sessionStatusArr;
                 _sampleView->scheduleddate=selectedDateArr;
                 gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
                 _sampleView->datearray=[[NSMutableArray alloc]init];
                 _sampleView->montharray=[[NSMutableArray alloc]init];
                 _sampleView->yeararray=[[NSMutableArray alloc]init];
                 
                 for(int i=0;i<[_sampleView->scheduleddate count];i++)
                 {
                     
                     NSDateComponents *components1 = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[_sampleView->scheduleddate objectAtIndex:i]];
                     [_sampleView->datearray addObject:[NSString stringWithFormat:@"%ld",(long)components1.day]];
                     [_sampleView->montharray addObject:[NSString stringWithFormat:@"%ld",(long)components1.month]];
                     [_sampleView->yeararray addObject:[NSString stringWithFormat:@"%ld",(long)components1.year]];
                 }
                 
                  [_sampleView setNeedsDisplay];
                
               

             }
             @catch (NSException *exception)
             {
                 NSLog(@"%@",exception.description);
             }
             
          
             
         }
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}



@end
