//
//  passwordViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 05/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "passwordViewController.h"

@interface passwordViewController ()
{
    UIView *paddingView;
    AppDelegate *app;
    BOOL loadercheck;
    Connection *conn;
}

@end

@implementation passwordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    loadercheck=FALSE;
    pointtrackcontentoffset=self.view.frame.origin;
    storerect=self.view.frame;
    screenBounds = [[UIScreen mainScreen] bounds];
    
    _underlinelabel.frame=CGRectMake((_buttonForgotPassword.frame.size.width-_buttonForgotPassword.frame.origin.x)/2, _underlinelabel.frame.origin.y, _underlinelabel.frame.size.width, _underlinelabel.frame.size.height);
    
      NSLog(@"Session name %@",app->customersessionname);
    
   
#pragma add padding
    for (id v in self.view.subviews)
    {
        UITextField *txtfield=(UITextField *)v;
        if([txtfield isKindOfClass:[UITextField class]])
        {
          paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
          txtfield.leftView = paddingView;
          txtfield.leftViewMode = UITextFieldViewModeAlways;
        }
    }
//#pragma modify Ui if things are saved in user default
//    if(app->userdefaultcheck)
//    {
//        _txtRepwd.hidden=TRUE;
//     
//        _txtRepwd.returnKeyType=UIReturnKeyDone;
//
//        // modify frame of login button
//        _btnLogin.frame=CGRectMake(_btnLogin.frame.origin.x,_txtRepwd.frame.origin.y+10, _btnLogin.frame.size.width, _btnLogin.frame.size.height);
//        
//        _buttonForgotPassword.hidden=FALSE;
//
//        _buttonForgotPassword.frame=CGRectMake(_buttonForgotPassword.frame.origin.x,_btnLogin.frame.origin.y+40, _buttonForgotPassword.frame.size.width, _buttonForgotPassword.frame.size.height);
//        
//        
//     
//    }else{
//        
//        _buttonForgotPassword.frame=CGRectMake(_buttonForgotPassword.frame.origin.x,_btnLogin.frame.origin.y+10, _buttonForgotPassword.frame.size.width, _buttonForgotPassword.frame.size.height);
//        
//        _buttonForgotPassword.hidden=TRUE;
//    }
#pragma modify Ui if things are saved in user default
    if(app->registerdfirst)
    {
        _txtRepwd.hidden=TRUE;
        _txtRepwd.returnKeyType=UIReturnKeyDone;
        
        // modify frame of login button
        _btnLogin.frame=CGRectMake(_btnLogin.frame.origin.x,_txtRepwd.frame.origin.y+10, _btnLogin.frame.size.width, _btnLogin.frame.size.height);
        _buttonForgotPassword.frame=CGRectMake(_buttonForgotPassword.frame.origin.x,_txtRepwd.frame.origin.y+_btnLogin.frame.size.height+10, _buttonForgotPassword.frame.size.width, _buttonForgotPassword.frame.size.height);
        
         _underlinelabel.frame=CGRectMake(_underlinelabel.frame.origin.x, _buttonForgotPassword.frame.origin.y+21, _underlinelabel.frame.size.width, _underlinelabel.frame.size.height);
    }
}

-(void)methodCall{
    

    
    [self webservicelogin];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapLogin:(id)sender
{
    [self dismisskeyboard];
    if(![NSString validation:_txtPwd.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if(!app->registerdfirst)
    {
        if(![NSString validation:_txtRepwd.text])
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill confirm password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            _txtRepwd.text=@"";
            return;
        }
        if([_txtPwd.text isEqualToString:_txtRepwd.text])
        {
            loadercheck=FALSE;
#pragma webservice for login
            [self webservicelogin];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Required" message:@"Please fill correct confirm password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            _txtRepwd.text=@"";
            return;
        }
    }
    else
    {
        //        if(![app->customerpwd isEqualToString:_txtPwd.text])
        //        {
        //
        //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Wrong" message:@"Please fill correct password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //            [alert show];
        //            _txtPwd.text=@"";
        //            return;
        //        }
        //        else
        //        {
        loadercheck=TRUE;
        conn=[[Connection alloc] init];
        conn.connectionDelegate=self;
        [conn tokenfetch];
        // }
    }
    
}
#pragma Webservice for sucessful password set
-(void)webservicelogin
{
    if(!loadercheck)
    {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [SVProgressHUD showWithStatus:@"Please wait.."];
    }
    NSString *path =[NSString stringWithFormat:@"%@setUserInfo",app->parentUrl];
    NSDictionary *params;
    if(app->registerdfirst)
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",_txtPwd.text,@"password",@"GET",@"type",nil];
    }
    else
    {
        params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",_txtPwd.text,@"password",@"SET",@"type",nil];
    }
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
         NSLog(@"%@",responseStr);
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
         NSLog(@"%@",dict);
         //here is place for code executed in success case
         if([[dict valueForKey:@"success"]integerValue]==0)
         {
             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[dict valueForKeyPath:@"result.message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             
             
         }
         else
         {
             if([[dict valueForKeyPath:@"result.message"]isEqualToString:@"Invalid password"])
             {
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"! Wrong" message:[dict valueForKeyPath:@"result.message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
                 [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                 [SVProgressHUD dismiss];
                 _txtPwd.text=@"";
                 return;
             }
             
             app->customerpwd=_txtPwd.text;
#pragma store username in user default
             NSUserDefaults *default1=[NSUserDefaults standardUserDefaults];
             if(!app->userdefaultcheck)
             {
                 [default1 setValue:app->customerusername forKey:@"customerusername"];
                 [default1 setValue:app->customertoken forKey:@"customertoken"];
                 [default1 setValue:app->customersessionname forKey:@"customersession"];
                 [default1 setValue:app->customeraccesskey forKey:@"customeraccesskey"];
                 [default1 setValue:app->MD5string forKey:@"MD5key"];
                 //[default1 setValue:app->consultantuserid forKey:@"consultantuserid"];
                 [default1 setBool:app->registerdfirst forKey:@"registered"];
                 
             }
             [default1 setValue:app->customerpwd forKey:@"customerpassword"];
             [default1 synchronize];
             app->userdefaultcheck=TRUE;
             _txtPwd.text=@"";
             _txtRepwd.text=@"";
#pragma send to dash board
             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
             [SVProgressHUD dismiss];
             [self performSegueWithIdentifier:@"segueToHome" sender:self];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}
-(void)sucessdone:(NSDictionary *)connectiondict
{
    if(!connectiondict)
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        return;
    }
#pragma webservice for login
    [self webservicelogin];
}

- (IBAction)didTapclearData:(id)sender
{
    app->customerusername=nil;
    app->customertoken=nil;
    app->customersessionname=nil;
    app->customerpwd=nil;
    app->customeraccesskey=nil;
    NSUserDefaults *default1=[NSUserDefaults standardUserDefaults];
    [default1 setValue:app->customerusername forKey:@"customerusername"];
    [default1 setValue:app->customertoken forKey:@"customertoken"];
    [default1 setValue:app->customersessionname forKey:@"customersession"];
    [default1 setValue:app->customerpwd forKey:@"customerpassword"];
    [default1 setValue:app->customeraccesskey forKey:@"customeraccesskey"];
    [default1 synchronize];
    app->userdefaultcheck=FALSE;
    
    // new code
    [self performSegueWithIdentifier:@"pushtorrotviewcontroller" sender:self];
    
}
#pragma textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    pointtrack =textField.frame.origin;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
#pragma add toolbar
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, 44)];
    toolBar.tag = 1000;
    toolBar.barTintColor=[UIColor colorWithRed:30/255.0f green:87/255.0f blue:140/255.0f alpha:1.0f];
    toolBar.tintColor=[UIColor whiteColor];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismisskeyboard)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [_txtPwd setInputAccessoryView:toolBar];
    [_txtRepwd setInputAccessoryView:toolBar];
    
#pragma Animate View
    CGRect rect=storerect;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if(screenBounds.size.height==480)
    {
        rect.origin.y-=pointtrack.y-110;
    }
    else if(screenBounds.size.height==568)
    {
        rect.origin.y-=pointtrack.y-180;
    }
    else if(screenBounds.size.height==667)
    {
        rect.origin.y-=pointtrack.y-160;
    }
    else if(screenBounds.size.height==736)
    {
        rect.origin.y-=pointtrack.y-270;
    }
    else
    {
        rect.origin.y-=pointtrack.y-180;
    }
    self.view.frame=rect;
    [UIView commitAnimations];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
#pragma manage next to text box functionality
    if(app->userdefaultcheck)
    {
    [self dismisskeyboard];
    }
    else
    {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self dismisskeyboard];
    }
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
-(void)dismisskeyboard
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
    [[self.view viewWithTag:1000] removeFromSuperview];
    [self.activeTextField resignFirstResponder];
}

//-(NSUInteger)supportedInterfaceOrientations
//{
//    if(isorientation)
//    {
//        return UIInterfaceOrientationMaskLandscapeRight|UIInterfaceOrientationLandscapeLeft;
//    }
//    return UIInterfaceOrientationMaskPortrait;
//}
- (IBAction)btnAction_ForgotPassword:(id)sender
{
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    
    NSString *path =[NSString stringWithFormat:@"%@setUserInfo",app->parentUrl];
    
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:app->customersessionname,@"sessionName",@"",@"password",@"FORGOTPASSWORD",@"type",nil];
    
       NSLog(@"Parameters: %@",params);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",responseStr);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
        NSLog(@"%@",dict);
#pragma store customer value in global dictionary
        //  app->globalcustomerdetail=[dict valueForKey:@"result"];
        //here is place for code executed in success case
        if([[dict valueForKey:@"success"]integerValue]==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"Password sent to your registered mobile number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
#pragma send to dash board
        //  [self performSegueWithIdentifier:@"segueToHome" sender:self];
        
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
         
         //here is place for code executed in success case
         [[UIApplication sharedApplication] endIgnoringInteractionEvents];
         [SVProgressHUD dismiss];
         NSLog(@"Error: %@", [error localizedDescription]);
         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];

    
    
}
@end
