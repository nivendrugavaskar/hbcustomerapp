//
//  scheduledetailsTableViewCell.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 23/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface scheduledetailsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *schedullabel;
@property (strong, nonatomic) IBOutlet UILabel *datelabel;
@property (strong, nonatomic) IBOutlet UILabel *datelabe11;
@property (strong, nonatomic) IBOutlet UIImageView *divider;
@property (strong, nonatomic) IBOutlet UIImageView *textarrow;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Address;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SessionStatus;

@end
