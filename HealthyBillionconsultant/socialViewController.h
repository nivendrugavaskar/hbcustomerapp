//
//  socialViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>



@interface socialViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>
{
    AppDelegate *app;
}
@property (strong, nonatomic) IBOutlet UITableView *aTableview;

@end
