//
//  socialViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 09/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "socialViewController.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>




@interface socialViewController ()
{
    NSArray *socialarray;
    float cellheight;
}

@end

@implementation socialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
#pragma cell height
    socialarray = @[@"Cell1",@"Cell2",@"Cell3"];
    cellheight=50.0;
    self.aTableview.layer.cornerRadius=5.0;
    if([socialarray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
    {
        self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, [socialarray count]*cellheight);
    }
    else
    {
    self.aTableview.frame=CGRectMake(self.aTableview.frame.origin.x, self.aTableview.frame.origin.y, self.aTableview.frame.size.width, self.view.frame.size.height-self.aTableview.frame.origin.y-5);
    }
    self.aTableview.layer.borderWidth=1.0;
    self.aTableview.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    
#pragma maintain scrroll view
    if([socialarray count]*cellheight<self.view.frame.size.height-self.aTableview.frame.origin.y)
    {
        self.aTableview.scrollEnabled=NO;
    }
    else
    {
        self.aTableview.scrollEnabled=YES;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = @"SOCIAL";
    [self setCustomNavigationButton];
    
    
}

#pragma cutomize navigation
-(void)setCustomNavigationButton
{
    
     [self.navigationController.navigationBar setCustomNavigationButton:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma tableviewdelegates and datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = [socialarray objectAtIndex:indexPath.row];
    UITableViewCell *cell;
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    for (UIView *v in cell.contentView.subviews)
    {
       
        if(!([v isKindOfClass:[UIImageView class]]||[v isKindOfClass:[UILabel class]]))
        {
            //NSLog(@"tableview width=%f",self.aTableview.frame.size.width);
           // NSLog(@"view size=%f",v.frame.size.width);
          //  v.backgroundColor=[UIColor redColor];
            v.frame=CGRectMake((self.aTableview.frame.size.width-v.frame.size.width)/2, v.frame.origin.y, v.frame.size.width, v.frame.size.height);
        }
    }
    
    
    @try
    {
    
    }
    @catch (NSException *exception)
    {
        NSLog(@"Description=%@",exception.description);
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellheight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [socialarray count];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        NSLog(@"indexpathrow=%ld",(long)indexPath.row);
    //    [self performSegueWithIdentifier:@"segueToAddFoodDiary" sender:indexPath];
    
    if (indexPath.row==0) {
        

        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            
            SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            
            [controller setInitialText:@"Healthy Billions\n"];
            [controller addURL:[NSURL URLWithString:@"https://www.facebook.com/HealthyBillions"]];
            [controller addImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://dev.businessprodemo.com/mBlame/hblogo.png"]]]];
            [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
                if (result == SLComposeViewControllerResultDone)
                {
                    NSLog(@"Posted");
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Posted Successfully" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                } else if (result == SLComposeViewControllerResultCancelled) {
                    NSLog(@"Post Cancelled");
                } else {
                    NSLog(@"Post Failed");
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Post Failed" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
            
            [self presentViewController:controller animated:YES completion:Nil];
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"! Not Available" message:@"Please setup your Facebook account" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }

        
    }
    else if (indexPath.row==1)
    {
        
        NSString *emailTitle = @"HBConsultant";
        NSString *messageBody = @"";
        NSArray *toRecipents = [NSArray arrayWithObject:@""];
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        if(mc)
        {
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
        }
        else
        {
//            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"! Not Available" message:@"Please setup your Email account" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
//            return;
        }
      }
    else if (indexPath.row==2)
    {
        NSLog(@"Link for rate us");
    }
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Mail Sent" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(BOOL)shouldAutorotate{
    
    return NO;
}

@end
