//
//  yogalistViewController.h
//  HealthyBillionconsultant
//
//  Created by Nivendru on 28/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import "MyRating.h"
#import "StarRatingView.h"

@protocol yogalistprotocol
- (void)buttonTappedfeedback;
@end

@interface yogalistViewController : UIViewController<startratingprotocol>
{
    AppDelegate *app;
}
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *buttonPfeedback;


@property (weak, nonatomic) IBOutlet UIButton *buttonFeedBack1;

@property (weak, nonatomic) IBOutlet UIButton *buttonFeedBack2;


@property (weak, nonatomic) IBOutlet UIButton *buttonFeedBack3;

@property (weak, nonatomic) IBOutlet UIButton *buttonFeedBack4;
@property (weak, nonatomic) IBOutlet UIButton *buttonFeedBack5;

@property (weak, nonatomic) IBOutlet UIButton *buttonOthers;
- (IBAction)btnAction_FB1:(id)sender;
- (IBAction)btnAction_FB2:(id)sender;

- (IBAction)btnAction_FB3:(id)sender;
- (IBAction)btnAction_FB4:(id)sender;

- (IBAction)btnAction_FB5:(id)sender;
- (IBAction)btnAction_others:(id)sender;

- (IBAction)btnAction_Needhelp:(id)sender;


@property (weak, nonatomic) IBOutlet UITextView *txtView_WriteComment;

@property (strong, nonatomic) IBOutlet StarRatingView *viewRating;


@property (strong, nonatomic) IBOutlet UILabel *yogalistlabel;
- (IBAction)didTappostfeedback:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnpostfeedback;
@property (weak, nonatomic) IBOutlet UIView *view_postcomment;
@property (strong, nonatomic) IBOutlet UITextView *activetxtview;

@property (strong,nonatomic) NSMutableArray *yogalistarray;
@property (strong,nonatomic) NSString *customerid,*sessionid;
@property (strong, nonatomic) IBOutlet UIView *dynamicview;
@property (strong, nonatomic) IBOutlet UIImageView *consulatnatimgview;
@property (strong, nonatomic) IBOutlet UILabel *consultantlabelname;

@property (assign) id <yogalistprotocol> delegateyoga;


@end
