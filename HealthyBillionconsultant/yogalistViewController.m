//
//  yogalistViewController.m
//  HealthyBillionconsultant
//
//  Created by Nivendru on 28/01/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "yogalistViewController.h"

#import "Rateone.h"



#define kLabelAllowance 50.0f
#define kStarViewHeight 15.0f
#define kStarViewWidth 75.0f
#define kLeftPadding 5.0f


#define kMyLabelAllowance 50.0f
#define kMyViewHeight 20.0f
#define kMyViewWidth 100.0f
#define kMyLeftPadding 5.0f


@interface yogalistViewController ()
{
    NSMutableArray *yogaarray;
    
    NSIndexPath *tickedclickindex;
    NSMutableDictionary *selectedIndexesyoga,*selectedstar,*postfeedbackdict,*yogalistdict;
    NSDictionary *dict;
    NSInteger pagenumber;
    BOOL enddata;
    
    CGRect screenBounds;
    CGPoint pointtrack,pointtrackcontentoffset;
    NSMutableArray *feedbackjsonarray;
    NSString *feedbacktypeString;
    
    int starcount;
    
    
}

@end

@implementation yogalistViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _viewRating.delegate=self;
    starcount=0;
    _dynamicview.hidden=FALSE;
    _dynamicview.backgroundColor=[UIColor colorWithRed:7/255.0f green:66/255.0f blue:122/255.0f alpha:1.0];

    NSLog(@"Yoga List Array %@",_yogalistarray);
    _consulatnatimgview.layer.cornerRadius=_consulatnatimgview.frame.size.width/2;
    _consultantlabelname.layer.cornerRadius=5.0;
    
    int sessionratingvalue=[[_yogalistarray valueForKey:@"session_ratings"] integerValue];
    
    //sessionratingvalue=0;
    
   // if (![_yogalistarray containsObject:@"session_ratings"])
     if (sessionratingvalue>0)
    
    {
        
        _buttonFeedBack1.enabled=FALSE;
        _buttonFeedBack2.enabled=FALSE;
        _buttonFeedBack3.enabled=FALSE;
        _buttonFeedBack4.enabled=FALSE;
        _buttonFeedBack5.enabled=FALSE;
        _buttonOthers.enabled=FALSE;
        _btnpostfeedback.enabled=FALSE;
        _txtView_WriteComment.editable=FALSE;
        
        // new code
        if(![[_yogalistarray valueForKey:@"comment"] isKindOfClass:[NSNull class]]||[[_yogalistarray valueForKey:@"comment"] isEqualToString:@""])
        {
            _txtView_WriteComment.text=[_yogalistarray valueForKey:@"comment"];
            if([_txtView_WriteComment.text isEqualToString:@""])
            {
                _txtView_WriteComment.text=@"NO COMMENTS";
            }
        }
        else
        {
            _txtView_WriteComment.text=@"NO COMMENTS";
        }
        
        if(!([[_yogalistarray valueForKey:@"conname"] isKindOfClass:[NSNull class]]||[[_yogalistarray valueForKey:@"conname"] isEqualToString:@""]))
        {
            _consultantlabelname.text=[_yogalistarray valueForKey:@"conname"];
        }
        else
        {
            _consultantlabelname.text=@"Consultant Name";
        }
        
        if(([[_yogalistarray valueForKey:@"consultantimage"] isKindOfClass:[NSNull class]]||[[_yogalistarray valueForKey:@"consultantimage"] isEqualToString:@""]))
        {
            _consulatnatimgview.image=[UIImage imageNamed:@"profile_img1.png"];
        }
        else
        {
            NSString *imagepath=[_yogalistarray valueForKey:@"consultantimage"];
            __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            activityIndicator.color=[UIColor redColor];
            activityIndicator.center = CGPointMake(CGRectGetMidX(_consulatnatimgview.bounds), CGRectGetMidY(_consulatnatimgview.bounds));
            activityIndicator.hidesWhenStopped = YES;
            
            __block BOOL successfetchimage=FALSE;
            __weak UIImageView *setimage = _consulatnatimgview;
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
            [_consulatnatimgview setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"profile_img1.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 setimage.contentMode=UIViewContentModeScaleAspectFill;
                 setimage.image=image;
                 // new code
                 
                 successfetchimage=TRUE;
                 [activityIndicator removeFromSuperview];
             }
                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 [activityIndicator removeFromSuperview];
             }];
            if(!successfetchimage)
            {
                if(![imagepath isEqualToString:@""]||imagepath)
                {
                    [_consulatnatimgview addSubview:activityIndicator];
                    [activityIndicator startAnimating];
                }
            }
            
            
        }
        
        // end
        
        _viewRating.userInteractionEnabled=NO;
              _viewRating=[_viewRating initWithFrame:_viewRating.frame andRating:(int)[[_yogalistarray valueForKey:@"session_ratings"] integerValue]*20 withLabel:NO animated:NO];
        _btnpostfeedback.hidden=YES;
        _viewRating.frame=CGRectMake(_viewRating.frame.origin.x, _viewRating.frame.origin.y+10, _viewRating.frame.size.width, _viewRating.frame.size.height);
        _dynamicview.hidden=FALSE;
        
        
    }
    else
    {
        
     //   MyRating *myRating=[[MyRating alloc]initWithFrame:CGRectMake(0, 0,kMyViewWidth+kMyLabelAllowance+kMyLeftPadding, kMyViewHeight) andRating:0 withLabel:NO animated:YES];
        
        _viewRating.userInteractionEnabled=YES;
        _viewRating=[_viewRating initWithFrame:_viewRating.frame andRating:sessionratingvalue*20 withLabel:NO animated:NO];
        
            _buttonFeedBack1.enabled=FALSE;
            _buttonFeedBack2.enabled=FALSE;
            _buttonFeedBack3.enabled=FALSE;
            _buttonFeedBack4.enabled=FALSE;
            _buttonFeedBack5.enabled=FALSE;
            _buttonOthers.enabled=FALSE;
            _txtView_WriteComment.editable=FALSE;
            _dynamicview.hidden=FALSE;
        
        // new code
        
        if(!([[_yogalistarray valueForKey:@"conname"] isKindOfClass:[NSNull class]]||[[_yogalistarray valueForKey:@"conname"] isEqualToString:@""]))
        {
            _consultantlabelname.text=[_yogalistarray valueForKey:@"conname"];
        }
        else
        {
            _consultantlabelname.text=@"Consultant Name";
        }
        
        if(([[_yogalistarray valueForKey:@"consultantimage"] isKindOfClass:[NSNull class]]||[[_yogalistarray valueForKey:@"consultantimage"] isEqualToString:@""]))
        {
            _consulatnatimgview.image=[UIImage imageNamed:@"profile_img1.png"];
        }
        else
        {
            NSString *imagepath=[_yogalistarray valueForKey:@"consultantimage"];
            __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            activityIndicator.color=[UIColor redColor];
            activityIndicator.center = CGPointMake(CGRectGetMidX(_consulatnatimgview.bounds), CGRectGetMidY(_consulatnatimgview.bounds));
            activityIndicator.hidesWhenStopped = YES;
            
            __block BOOL successfetchimage=FALSE;
            __weak UIImageView *setimage = _consulatnatimgview;
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:imagepath]];
            [_consulatnatimgview setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"profile_img1.png"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
             {
                 setimage.contentMode=UIViewContentModeScaleAspectFill;
                 setimage.image=image;
                 // new code
                 
                 successfetchimage=TRUE;
                 [activityIndicator removeFromSuperview];
             }
                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
             {
                 [activityIndicator removeFromSuperview];
             }];
            if(!successfetchimage)
            {
                if(![imagepath isEqualToString:@""]||imagepath)
                {
                    [_consulatnatimgview addSubview:activityIndicator];
                    [activityIndicator startAnimating];
                }
            }
            
            
        }
        
        // end
       
    }
    
    
    
    
    self.view_postcomment.layer.cornerRadius=5.0;
    self.view_postcomment.layer.borderWidth=1.0;
    self.view_postcomment.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    
    
    app=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.title=@"FEEDBACK";
    
    enddata=FALSE;
    pagenumber=0;
    self.yogalistlabel.layer.borderWidth=1.0;
    self.yogalistlabel.layer.borderColor=[[UIColor colorWithRed:132/255.0f green:166/255.0f blue:173/255.0f alpha:1.0f] CGColor];
    self.yogalistlabel.clipsToBounds=YES;
    self.yogalistlabel.layer.cornerRadius=5.0;
    
    self.navigationController.interactivePopGestureRecognizer.enabled=NO;
    [self.navigationItem setHidesBackButton:YES];
    // add back button
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"back_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    UIBarButtonItem *backbarbutton = [[UIBarButtonItem alloc] initWithCustomView:custombutton];
    backbarbutton.style = UIBarButtonItemStylePlain;
    self.navigationItem.leftBarButtonItems = @[backbarbutton];
    
}

#pragma delegate method from starview

-(void)sendstarvalue:(int)sender :(int)index
{
   // NSLog(@"Touched index%d",sender);
    starcount=sender/20;
    if(starcount==5)
    {
        if(_dynamicview.hidden==YES)
        {
             _dynamicview.hidden=NO;
             _txtView_WriteComment.editable=TRUE;
        }
    }
    else
    {
        //[_dynamicview removeFromSuperview];
        _dynamicview.hidden=YES;
    }
    
     _viewRating=[_viewRating initWithFrame:_viewRating.frame andRating:starcount*20 withLabel:NO animated:NO];
    
    
    if (starcount>0&&starcount<5)
    {
        
        _buttonFeedBack1.enabled=TRUE;
        _buttonFeedBack2.enabled=TRUE;
        _buttonFeedBack3.enabled=TRUE;
        _buttonFeedBack4.enabled=TRUE;
        _buttonFeedBack5.enabled=TRUE;
        _buttonOthers.enabled=TRUE;
        _txtView_WriteComment.editable=FALSE;
        _dynamicview.hidden=TRUE;
        _txtView_WriteComment.text=@"Write Comment";
        
    }
    else
    {
        _buttonFeedBack1.enabled=FALSE;
        _buttonFeedBack2.enabled=FALSE;
        _buttonFeedBack3.enabled=FALSE;
        _buttonFeedBack4.enabled=FALSE;
        _buttonFeedBack5.enabled=FALSE;
        _buttonOthers.enabled=FALSE;
        if(starcount==5)
        {
        _txtView_WriteComment.editable=TRUE;
        }
        else
        {
           _txtView_WriteComment.editable=FALSE;
        }
        _dynamicview.hidden=FALSE;
    }
    
   
}





-(void)viewWillAppear:(BOOL)animated
{
    screenBounds=[[UIScreen mainScreen] bounds];
    pointtrackcontentoffset=self.view.frame.origin;
    
    
}
#pragma mark -- TextViewDelegate
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@"Write Comment"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    self.activetxtview=textView;
    pointtrack = textView.frame.origin;
    
    CGRect rect=self.view.frame;
    rect.origin=pointtrackcontentoffset;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    // if(screenBounds.size.height==480)
    // {
    rect.origin.y-=pointtrack.y+190;
    // }
    self.view.frame=rect;
    [UIView commitAnimations];
    
    
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Write Comment";
        textView.textColor = [UIColor lightGrayColor];
    }
    
    
    
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        // [textView resignFirstResponder];
        
        [self dismisskeyboard];
        return NO;
    }
    
    return YES;
}

-(void)dismisskeyboard
{
    CGRect rect=self.view.frame;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    rect.origin.y=pointtrackcontentoffset.y;
    self.view.frame=rect;
    [UIView commitAnimations];
    [self.activetxtview resignFirstResponder];
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
    if(buttonIndex == 0)
    {
        [self back];
    }
    }
    
}



-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (BOOL)cellIsSelected:(NSIndexPath *)indexPath
{
    
    if([[selectedIndexesyoga valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]isEqualToString:@"select"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}



- (void)viewDidUnload
{
    selectedIndexesyoga = nil;
    selectedstar=nil;
    [super viewDidUnload];
}
- (IBAction)didTappostfeedback:(id)sender
{
    [self dismisskeyboard];
    if(starcount==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Feedback Required" message:@"Please give a feedback" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    else if(starcount<5&&[_txtView_WriteComment.text isEqualToString:@"Write Comment"])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Comment Required" message:@"Please give a Comment" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    
    NSString *path =[NSString stringWithFormat:@"%@postFeedbackByCustomer",app->parentUrl];
    
    
   
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            app->customersessionname,@"sessionName",
                            [_yogalistarray valueForKey:@"sessionsid"],@"sessionid",
                            [_yogalistarray valueForKey:@"sessionsid"],@"consultantid"
                             ,[NSString stringWithFormat:@"%d",starcount],@"rating"
                             ,[_yogalistarray valueForKey:@"accountid"],@"accountid",
                            feedbacktypeString,@"feedback_type",
                            _txtView_WriteComment.text,@"comment",
                            nil];
    
    
    
    
    NSLog(@"Parameters: %@",params);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",responseStr);
        dict = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:NULL];
      
#pragma store customer value in global dictionary
        //  app->globalcustomerdetail=[dict valueForKey:@"result"];
        //here is place for code executed in success case
        if([[dict valueForKey:@"success"]integerValue]==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[[dict valueForKey:@"error"] valueForKey:@"code"] message:[[dict valueForKey:@"error"] valueForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else
        {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Success" message:@"FeedBack Submitted Successfully!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            // new code
            [self.delegateyoga buttonTappedfeedback];
            alert.tag=100;
            [alert show];
            
        }
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];

        
        
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
        //here is place for code executed in success case
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [SVProgressHUD dismiss];
        NSLog(@"Error: %@", [error localizedDescription]);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[error localizedDescription] message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];
    
    
    
    
    
}




- (IBAction)btnAction_FB1:(id)sender
{
    [self dismisskeyboard];
    // new code
    _buttonFeedBack1.superview.backgroundColor=[UIColor lightGrayColor];
    _buttonFeedBack2.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack3.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack4.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack5.superview.backgroundColor=[UIColor clearColor];
    _buttonOthers.superview.backgroundColor=[UIColor clearColor];
    
    _txtView_WriteComment.text=@"FeedBack1";
    
    feedbacktypeString=@"FeedBack1";
    
    _txtView_WriteComment.editable=FALSE;
    
}

- (IBAction)btnAction_FB2:(id)sender {
    
    [self dismisskeyboard];
    // new code
    _buttonFeedBack1.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack2.superview.backgroundColor=[UIColor lightGrayColor];
    _buttonFeedBack3.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack4.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack5.superview.backgroundColor=[UIColor clearColor];
    _buttonOthers.superview.backgroundColor=[UIColor clearColor];
    
     _txtView_WriteComment.text=@"FeedBack2";
    
     feedbacktypeString=@"FeedBack2";
    
    _txtView_WriteComment.editable=FALSE;

}

- (IBAction)btnAction_FB3:(id)sender {
    // new code
    [self dismisskeyboard];
    _buttonFeedBack1.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack2.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack3.superview.backgroundColor=[UIColor lightGrayColor];
    _buttonFeedBack4.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack5.superview.backgroundColor=[UIColor clearColor];
    _buttonOthers.superview.backgroundColor=[UIColor clearColor];
    
     _txtView_WriteComment.text=@"FeedBack3";
    
     feedbacktypeString=@"FeedBack3";
    
    _txtView_WriteComment.editable=FALSE;

}

- (IBAction)btnAction_FB4:(id)sender {
    // new code
    [self dismisskeyboard];
    _buttonFeedBack1.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack2.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack3.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack4.superview.backgroundColor=[UIColor lightGrayColor];
    _buttonFeedBack5.superview.backgroundColor=[UIColor clearColor];
    _buttonOthers.superview.backgroundColor=[UIColor clearColor];
    
     _txtView_WriteComment.text=@"FeedBack4";
    
     feedbacktypeString=@"FeedBack4";
    
    _txtView_WriteComment.editable=FALSE;

}

- (IBAction)btnAction_FB5:(id)sender {
    
    [self dismisskeyboard];
    // new code
    _buttonFeedBack1.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack2.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack3.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack4.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack5.superview.backgroundColor=[UIColor lightGrayColor];
    _buttonOthers.superview.backgroundColor=[UIColor clearColor];
    
     _txtView_WriteComment.text=@"FeedBack5";
     feedbacktypeString=@"FeedBack5";
    
    _txtView_WriteComment.editable=FALSE;

    
}

- (IBAction)btnAction_others:(id)sender {
    
    // new code
    [self dismisskeyboard];
    _buttonFeedBack1.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack2.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack3.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack4.superview.backgroundColor=[UIColor clearColor];
    _buttonFeedBack5.superview.backgroundColor=[UIColor clearColor];
    _buttonOthers.superview.backgroundColor=[UIColor lightGrayColor];
    
     _txtView_WriteComment.text=@"Write Comment";
    
    
     feedbacktypeString=@"others";
    
    _txtView_WriteComment.editable=TRUE;

    
}

- (IBAction)btnAction_Needhelp:(id)sender {
}
@end
