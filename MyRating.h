//
//  MyRating.h
//  Baarfi
//
//  Created by Arnab on 27/01/15.
//  Copyright (c) 2015 Arnab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#pragma GLOBAL STAR VALUE
NSString *starvalue;

@protocol StarvalueGet <NSObject>

-(void)getStarValue;

@end

@interface MyRating : UIView

- (id)initWithFrame:(CGRect)frame andRating:(int)rating withLabel:(BOOL)label animated:(BOOL)animated;
@property(nonatomic,retain)id<StarvalueGet>delegate;

@property (nonatomic) int userRating;
@property (nonatomic) int maxrating;
@property (nonatomic) int rating;
@property (nonatomic) BOOL animated;
@property (nonatomic) float kLabelAllowance;
@property (nonatomic,strong) NSTimer* timer;
@property (nonatomic,strong) UILabel* label;
@property (nonatomic,strong) CALayer* tintLayer;
@end
