//
//  Rateone.h
//  HealthyBillionCustomer
//
//  Created by Arnab on 03/04/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Rateone : UIView

- (id)initWithFrame:(CGRect)frame andRating:(int)rating withLabel:(BOOL)label animated:(BOOL)animated;
@end


