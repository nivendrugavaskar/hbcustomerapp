//
//  customdatepicker.m
//  HealthyBillionCustomer
//
//  Created by Nivendru on 03/04/15.
//  Copyright (c) 2015 NivendruGavaskar. All rights reserved.
//

#import "customdatepicker.h"

@implementation customdatepicker

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if(self)
    {
        self.delegate=self;
        self.dataSource=self;
        
        
        NSDate *today = [[NSDate alloc] init];
        NSCalendar *calendar1 = [NSCalendar currentCalendar];
        NSDateComponents *comps1 = [[NSDateComponents alloc] init];
        comps1= [calendar1 components:NSCalendarUnitYear | NSCalendarUnitMonth fromDate:today];
        NSInteger year=comps1.year;
        montharray=[[NSMutableArray alloc]initWithObjects:@"January",@"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December", nil];
        
        yeararray=[[NSMutableArray alloc]init];
        for (int i=2014; i<=year; i++)
        {
            [yeararray addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        
        [self reloadAllComponents];
//        [self selectRow:month-1 inComponent:0 animated:YES];
//        if(month==12)
//        {
//        [self selectRow:0 inComponent:1 animated:YES];
//        }
//        else
//        {
//        [self selectRow:1 inComponent:1 animated:YES];
//        }
        
    }
    return self;
}

#pragma mark Picker Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component==0)
    {
    return [NSString stringWithFormat:@"%@", [montharray objectAtIndex:row]];
    }
    return [NSString stringWithFormat:@"%@", [yeararray objectAtIndex:row]];
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component==0)
    {
        return [montharray count];
    }
    return [yeararray count];
    
}


@end
